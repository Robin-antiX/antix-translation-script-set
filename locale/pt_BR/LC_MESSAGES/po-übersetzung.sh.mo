��          �      L      �  (   �     �  $   
     /    ?  �   W  H     '   ]  "   �  9   �  *   �  �     ?     8   L  <   �     �     �     �  9  �  4   *  *   _  *   �      �  �   �  �   �	  V   �
  /   �
  >     +   \  3   �     �  X   �  F     C   ]     �     �     �           
                                                                                     	    Der Übersetzungsdienst antwortet nicht. Die Übersetzung wurde beendet. Die Übersetzung wurde unterbrochen. Fortsetzung in  In $k Sekunden wird der Versuch automatisch wiederholt. Falls der Dienst\nweiterhin nicht antwortet, versuchen eine andere IP Adresse zu erhalten\nund drücken anschließend die Taste »w«. Falls sie die Übersetzung\nabbrechen möchten drücken Sie zum Beenden die Taste »b«. Kein Übersetzungsmuster gefunden. Bitte erstellen Sie die Datei
  ./$projektkennung/$ressourcenkennung/locale/$prefix.$pot_datei
aus der original .pot Datei und starten das Script erneut. Keine .pot Datei gefunden. Hole aktuelle .pot-Datei vom transifex Server Lese .pot-Datei: $anzahl_zeilen Zeilen. Sekunden. Taste »b« zum Beenden. Sprache ist bereits zu 100% übersetzt. Wird übergangen. Verarbeite po-Datei, $anzahl_zeilen Zeilen Verwendung: po-übersetzung.sh <transifex-projekt-„slug”> <transifex-ressourcen-sprachbezeichnung> <transifex-ressourcen-„slug”> <wahre-ressourcen-sprache> [dryrun]
Nur der letzte Parameter (dryrun) ist optional, die Reihenfolge ist einzuhalten.
 Zum Fortsetzen mit der nächsten Sprache Skript erneut starten. Zum Fortsetzen mit dieser Sprache Skript erneut starten. \nÜbertrage komplettierte Übersetzung zum transifex Server abgeschlossen. Übersetze zu Übersetzung zu  Project-Id-Version: po-übersetzung.sh Ver. 0.64
Report-Msgid-Bugs-To: forum.antiXlinux.com
PO-Revision-Date: 2021-10-25 18:34+0200
Last-Translator: Robin.antiX
Language-Team: antiX-contribs (transifex.com)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 O prestador de serviços de tradução não responde O processo de tradução foi interrompido. O processo de tradução foi interrompido. O próximo idioma começará em  A repetição automática começará em $k segundos. Caso o serviço ainda não res-\nponda, tente obter um novo endereço IP e então pressione a tecla “w”. Caso\nvocê queira abortar o processo de tradução, pressione o botão “b” para sair. Nenhum padrão de tradução encontrado. Favor criar o arquivo
  ./$projektkennung/$ressourcenkennung/locale/$prefix.$pot_datei
a partir do arquivo .pot original e iniciar este script novamente. Nenhum arquivo .pot encontrado. Buscando o recente arquivo .pot do servidor transifex. Leitura do arquivo .pot: $anzahl_zeilen linhas. segundos. Pressione o botão “b” se você quiser desistir. O idioma já está 100% traduzido. Ignorar. Processamento do arquivo .po, $anzahl_zeilen linhas Utilização: po-übersetzung.sh <transifex-projeto-„slug”> <transifex-resource-idioma-ID> <transifex-resource-„slug”> <linguagem-verdadeira-de-resource> [dryrun]
Apenas o último argumento (dryrun) é opcional, a ordem dos argumentos é rigorosa.
 A fim de continuar com a próxima linguagem não traduzida, favor reiniciar este script. A fim de continuar com a linguagem atual, basta reiniciar este script. \nTransferência de tradução concluída para o servidor transifex foi finalizada. Traduzindo para A tradução para o idioma  