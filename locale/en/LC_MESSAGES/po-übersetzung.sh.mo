��          �      l      �  (   �     
  $   *  #   O     s  &   �    �  �   �  H   z  '   �  "   �  9     *   H    s  ?   �  8   �  <        @     O     ]  >  n  -   �      �      �  #   	     A	  1   ^	  �   �	  �   v
  G   !  (   i  0   �  2   �  (   �      P   7  M   �  8   �          #     2                  
                               	                                                 Der Übersetzungsdienst antwortet nicht. Die Übersetzung wurde beendet. Die Übersetzung wurde unterbrochen. Erzeuge Dateien im git-Verzeichnis. Fortsetzung in  Hole Übersetzung vom transifex-Server In $k Sekunden wird der Versuch automatisch widerholt. Falls der Dienst\nweiterhin nicht antwortet, versuchen eine andere IP Adresse zu erhalten\nund drücken anschließend die Taste »w«. Falls sie die Übersetzung\nabbrechen möchten drücken Sie zum Beenden die Taste »b«. Kein Übersetzungsmuster gefunden. Bitte erstellen Sie die Datei
  ./$projektkennung/$ressourcenkennung/po/$prefix.$pot_datei
aus der original .pot Datei und starten das Script erneut. Keine .pot Datei gefunden. Hole aktuelle .pot-Datei vom transifex Server Lese .pot-Datei: $anzahl_zeilen Zeilen. Sekunden. Taste »b« zum Beenden. Sprache ist bereits zu 100% übersetzt. Wird übergangen. Verarbeite po-Datei, $anzahl_zeilen Zeilen Verwendung: po-übersetzung.sh <transifex-Organisationskennung> <transifex-Projektkennung> <transifex-Ressourcen-Sprachbezeichnung> <transifex-Ressourcenkennung> <Wahre-Ressourcensprache> [dryrun]
Nur der letzte Parameter (dryrun) ist optional, die Reihenfolge ist einzuhalten.
 Zum Fortsetzen mit der nächsten Sprache Skript erneut starten. Zum Fortsetzen mit dieser Sprache Skript erneut starten. \nÜbertrage komplettierte Übersetzung zum transifex Server abgeschlossen. Übersetze zu Übersetzung zu  Project-Id-Version: po-übersetzung.sh Ver. 0.67n4
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-01-09 01:15+0100
Last-Translator: Robin.antiX
Language-Team: antiX-contribs (transifex.com)
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0.1
 Translation service provider doesn't respond. Translation process was stopped. Translation process was stopped. Creating files in git subdirectory. Next language will start in  Fetching recent translation from transifex server Automatically retry will start in $k seconds. In case the service\nstill doesn't respond, please try to get a fresh IP address and\nthen press key »w«. In case you want to abort translation process,\npress button »b« to quit. No Translation pattern found. Please create the File
  ./$projektkennung/$ressourcenkennung/po/$prefix.$pot_datei
from the original .pot file and start this script again. No .pot file found. Fetching the recent .pot file from transifex server Reading .pot file: $anzahl_zeilen lines. seconds. Press button »b« if you want to quit. The language is 100% translated already. Skipping. Processing po file, $anzahl_zeilen lines Usage: po-übersetzung.sh <transifex-organisation-„slug”> <transifex-project-„slug”> <transifex-resource-language-ID> <transifex-resource-„slug”> <true-langauge-of-resource> [dryrun]
Only the last argument (dryrun) is optional, the sort order of arguments is strict.
 In order to continue with next untranslated language please restart this script. In order to continue with current language please simply restart this script. \nTransferring completed Translation to transifex server has been finnished. Translating to Translation to language  