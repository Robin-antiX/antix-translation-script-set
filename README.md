# antiX-translation-script-set

Translation script collection for internal use, antiX community

Written by Robin for antiX community, 2021

License: GPL v.3

Please be carefully when applying these scripts, they don't contain any security checking preventing you from accidentally deleting/overwriting existing files or directories in case of misconfiguration. Use on your own risk.

Project-Status: Early testing phase.

Please note: all these scripts were written for internal usage only in German language. English language users please put the respective .mo file from /locale into the /usr/share/locale/en/LC_MESSAGES folder of your system before starting. All the comments within the scripts have been translated to English.

-----

You may use any source language in your program files, you are not restricted to English anymore (this is true by now only for antiX-contribs, utilising the universal source language with its transifex placeholder »en@pirate«). Just apply the respective command line arguments when starting the translation script. Please read usage instructions within scripts for detailed information about configuration and workflow.

- for html/xhtml files (including .pdf creation from these):
Apply _text_vorbereiten.sh_ and _text_übersetzen.sh_ on a specially prepared copy of your original (x)html file. These scripts and also the upload to transifex after translation will fail on invalid html/xhtml code in base files, so please check carefully before starting. Manual upload to transifex only.

- for gettext .pot/.po → .mo files
Apply _po-übersetzung.sh_ on a specially prepared copy of your .pot file. Automatic upload to transifex. See workflow instructions inside script for details.

- for .desktop files (work in progress, not ready yet)

You'll need having »transifex cli« and »translate-shell« being installed.

-----

http://www.forum.antixlinux.com
