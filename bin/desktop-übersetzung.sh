#!/bin/bash

# desktop-übersetzung.sh Version 0.56
#
# License: GPL v.3
# Written (c) 2021 by Robin.antiX for antiX-Community
#
# Tiny script for preparation, uploading and harvesting translations of
# translatable strings present in .desktop files at and from transifex.
# No gitlab upload support for the translated .desktop files by now.
# usage: desktop-übersetzung.sh <extract|merge|translate> <transifex-project-"slug"> <transifex-resource-language-name> <transifex-resource-"slug"> <true-resource-language> [dryrun]
# The language list file "sprachen.lst" is meant to have full control 
# which languages are uploaded to transifex, since there are languages
# found within some .desktop files we don't have on transifex by now.
# !! Use this script carefully! No checks are performed to prevent damage
#  while feeding wrong parameters or starting from a wrong place.)
# Hint: this script set was written in German language originally, but it
# is meanwhile translated to English language.
# English language users please install the respective .mo language file
# from /locale folder to /usr/share/locale/en/LC_MESSAGES
# folder of your system to make the script running in English language.
# You may change the file and folder names according to your needs in the
# section "Configurable variables" below.

# --------------------Konfigurierbare Variablen (Configurable variables) -------------------------
sprachenliste="sprachen.lst"			# list of language identifiers, one entry per line, 2 letter code allowed (and some 4 significant letter also, depends on translation service used).
quellverzeichnis="./sources"			# Verzeichnis, in das alle gefundenen .desktop Dateien zur Übersetzung kopiert werden (this is the folder where all processed .desktop files are locally stored. No upload to gitlab by now.)
# -------------------------------------------------------------------------------------------------

# Einlesen der Befehlszeilenargumente
# read command line arguments
if [ "$1" == "extract" ] || [ "$1" == "merge" ] || [ "$1" == "translate" ]; then
	:;
else
	echo $"Verwendung: desktop-übersetzung.sh <extract|merge|translate> <transifex-projekt-„slug”> <transifex-ressourcen-sprachbezeichnung> <transifex-ressourcen-„slug”> <wahre-ressourcen-sprache> [dryrun]
Nur der letzte Parameter (dryrun) ist optional, die Reihenfolge ist einzuhalten.
"
	exit 1
fi
dry_run=false
projektkennung="$2"
transifex_ressourcenbezeichnung="$3"
ressourcenkennung="$4"
ausgangssprache="$5"
if [ -n "$6" ]; then [ "$6" == "dryrun" ] && dry_run=true ; fi


# Grundstruktur für die Ressorche bereitstellen (create base structure for ressource)
ausgabedatei="$ressourcenkennung.strings"
ausgabeverzeichnis="./locale"
ausgabe_name="$(echo $ausgabedatei | rev | cut -d. -f2- | rev)"
ausgabe_erweiterung="$(echo $ausgabedatei | rev | cut -d. -f1 | rev)"
mkdir -p "./$projektkennung/$ressourcenkennung/.tx"

# Erzeugen der ressourcenspezifischen Konfigurationsdatei für den transifex client
# (create resource specific tx configuration file)
echo "[main]
host = https://www.transifex.com

[$projektkennung.$ressourcenkennung]
file_filter = locale/$ausgabe_name.<lang>.$ausgabe_erweiterung
minimum_perc = 0
source_file = ./$ausgabedatei
source_lang = $transifex_ressourcenbezeichnung
type = STRINGS
" > "$projektkennung/$ressourcenkennung/.tx/config"

# Alle folgenden Befehle im Transifex-Arbeitsverzeichnis ausführen, um den tx-client zu befriedigen
# (run all following commands from within transifex working directory to make tx client happy.)
arbeitsverzeichnis="$(pwd)"
cd  "./$projektkennung/$ressourcenkennung"

mkdir -p "$quellverzeichnis"
mkdir -p "$ausgabeverzeichnis"

renice -n 19 -p $$  # let's work in background

# Aufräumen beim verlassen des Scriptes vorbereiten
# (prepare cleanup on exit)
function cleanup {
	rm -f ./tmp_$$/*
	[ -e ./tmp_$$ ] && rmdir ./tmp_$$
}
trap cleanup EXIT

# Teil 1: Extrahieren der übersetzbaren Zeichenketten aus den .desktop-Dateien, die in einem laufenden antiX-System im Dateisystem-zweig "/usr" vorhanden sind. (sinnvollerweise dafür die Version "full" verwenden, um alles standardprogramme erfassen)
# (Part 1: Extract all the translatable Strings from .desktop files present on running antiX in file system branch "/usr). (Use "full" version of antiX for this in order to get strings from all default programs.)
function extract {
# *** todo: Eine Vorhandene Source-Datei von Transifex herunterladen, um sicherzustellen, daß beim Hinzufügen weiterer .pot-Dateien keine vorhandenen Strings verloren gehen. 
# *** todo: Strings in dieser Datei mit den neu gefundenen abgleichen, dmait keine Duplikate entstehen. Inhalt identischer Stringidentifikatoren ersetzen, falls neuer Inhalt vorhanden ist.
> "$ausgabedatei"

# Analyse der ".desktop"-Dateien (Analysing of .desktop files)
echo $"Analysiere .desktop-Dateien"
while IFS= read -r dateiname; do
	n=1
	if [ -f "$dateiname" ]; then
		cp "$dateiname" "$quellverzeichnis"								# copy source file to processing folder
		anzahl_zeilen="$(sed -n "$=" "$dateiname")"
		dateikennung="$(echo "$dateiname" | rev | cut -d/ -f1 | cut -d. -f2- | rev)"
echo $"Verarbeite Datei:"" $dateikennung"
		abschnitt=""		
		while [ $n -le $anzahl_zeilen ]; do
			zeile="$(sed -n ${n}p "$dateiname")"						# read file line by line
			if echo "$zeile" | grep "^\[" > /dev/null; then
				abschnitt="$(echo "$zeile" | sed 's/\[\(..*\)\]/\1/')"
			elif echo "$zeile" | grep "^Name=" > /dev/null; then		# extract original "Name=" entry
				eintrag_name="$(echo "$zeile" | cut -d= -f2- | sed 's/"/“/g')"
				echo "/* File or program name: . . . . . . . $dateikennung" >> "$ausgabedatei"
				echo "Section within .desktop file: . . $abschnitt" >> "$ausgabedatei"
				echo "Entry type: . . . . . . . . . . . . . . . . . Name=" >> "$ausgabedatei"
				echo "This entry will be displayed e.g. in menus. Keep it as short as possible, please. */" >> "$ausgabedatei"
				echo -e "\x22$dateikennung|$abschnitt|Name\x22 = \x22$eintrag_name\x22;\n" >> "$ausgabedatei"
			elif echo "$zeile" | grep "^GenericName=" > /dev/null; then	# extract original "GenericName=" entry
				eintrag_generischer="$(echo "$zeile" | cut -d= -f2- | sed 's/"/“/g')"
				echo "/* File or program name: . . . . . . . $dateikennung" >> "$ausgabedatei"
				echo "Section within .desktop file: . . $abschnitt" >> "$ausgabedatei"
				echo "Entry type: . . . . . . . . . . . . . . . . . GenericName=" >> "$ausgabedatei"
				echo "This entry may be additionally displayed e.g. in menus. */" >> "$ausgabedatei"
				echo -e "\x22$dateikennung|$abschnitt|GenericName\x22 = \x22$eintrag_generischer\x22;\n" >> "$ausgabedatei"
			elif echo "$zeile" | grep "^Comment=" > /dev/null; then		# extract original "Comment=" entry
				eintrag_kommentar="$(echo "$zeile" | cut -d= -f2- | sed 's/"/“/g')"
				echo "/* File or program name: . . . . . . . $dateikennung" >> "$ausgabedatei"
				echo "Section within .desktop file: . . $abschnitt" >> "$ausgabedatei"
				echo "Entry type: . . . . . . . . . . . . . . . . . Comment=" >> "$ausgabedatei"
				echo "This entry may be displayed e.g. as tooltip in menus. You may explain here the main function of the program in short words, it is a good idea to use some additional synonyms within this entry. */" >> "$ausgabedatei"				
				echo -e "\x22$dateikennung|$abschnitt|Comment\x22 = \x22$eintrag_kommentar\x22;\n" >> "$ausgabedatei"
			elif echo "$zeile" | grep "^Name\[" > /dev/null; then		# extract language specific "Name=" entries
				sprache="$(echo "$zeile" | sed 's/Name\[\(..*\)\]=..*/\1/')"
				eintrag_name="$(echo "$zeile" | cut -d= -f2- | sed 's/"/“/g')"
				echo -e "\x22$dateikennung|$abschnitt|Name\x22 = \x22$eintrag_name\x22;\n" >> "$ausgabeverzeichnis/$ausgabe_name.$sprache.$ausgabe_erweiterung"
			elif echo "$zeile" | grep "^GenericName\[" > /dev/null; then	# extract language specific "GenericName=" entries
				sprache="$(echo "$zeile" | sed 's/GenericName\[\(..*\)\]=..*/\1/')"
				eintrag_generischer="$(echo "$zeile" | cut -d= -f2- | sed 's/"/“/g')"
				echo -e "\x22$dateikennung|$abschnitt|GenericName\x22 = \x22$eintrag_generischer\x22;\n" >> "$ausgabeverzeichnis/$ausgabe_name.$sprache.$ausgabe_erweiterung"
			elif echo "$zeile" | grep "^Comment\[" > /dev/null; then	# extract language specific "Comment=" entries
				sprache="$(echo "$zeile" | sed 's/Comment\[\(..*\)\]=..*/\1/')"
				eintrag_kommentar="$(echo "$zeile" | cut -d= -f2- | sed 's/"/“/g')"
				echo -e "\x22$dateikennung|$abschnitt|Comment\x22 = \x22$eintrag_kommentar\x22;\n" >> "$ausgabeverzeichnis/$ausgabe_name.$sprache.$ausgabe_erweiterung"
			fi
			let $((n++))
		done
	fi
done < <(find /usr -type f -name *.desktop)

# Konvertiere zum von transifex erwarteten Dateiformat
# (convert to expected file format)
for datei in $ausgabeverzeichnis/*.strings; do
	iconv -f UTF-8 -t UTF-16 < "$datei" > "$datei"_16
	rm -f "$datei"
	mv "$datei"_16 "$datei"
done
iconv -f UTF-8 -t UTF-16 < "$ausgabedatei" > "$ausgabedatei"_16
rm -f "$ausgabedatei"
mv "$ausgabedatei"_16 "$ausgabedatei"


# Neues masterfile und sprachspezifische Dateien zum transifex server hochladen
# (transfer translated .po file to transifex)
echo -e $"\nÜbertrage .strings-Dateien zum transifex Server"
if ! $dry_run; then
	tx push -r "$projektkennung.$ressourcenkennung" -s --no-interactive
	while read -r zielsprache; do
		if [ -e "$ausgabeverzeichnis/$ausgabe_name.$zielsprache.$ausgabe_erweiterung" ]; then
			tx push -l "$zielsprache" -r "$projektkennung.$ressourcenkennung" -t --no-interactive
		fi
	done < "$arbeitsverzeichnis/$sprachenliste"
fi
}


# Teil 2: Abholen der Übersetzungen von Transifex und verteilen der Einträge in die .desktop-Dateien im Quellordner aus Teil 1)
# (Part 1: Harvest translations from transifex and merge them into the .desktop folders stored in part 1)
function merge {
ausgabeverzeichnis="${ausgabeverzeichnis:2}"

# hole die übersetzten string- dateien vom transifex-server und den master string file
# (fetch master file and translation strings from transifex)
tx pull -f -r "$projektkennung.$ressourcenkennung" -s --no-interactive
tx pull -f -r "$projektkennung.$ressourcenkennung" --all --no-interactive

# Dateien in temporäres Arbeitsverzeichnis kopieren und zu utf-8 konvertieren
temporaerverzeichnis="./tmp_$$"
mkdir -p "$temporaerverzeichnis/$ausgabeverzeichnis"

# convert back to linux standard from expected file format
for datei in $ausgabeverzeichnis/*.strings; do
	iconv -f UTF-16 -t UTF-8 < "$datei" > "$temporaerverzeichnis/$datei"
done
iconv -f UTF-16 -t UTF-8 < "$ausgabedatei" > "$temporaerverzeichnis/$ausgabedatei"

for dateiname in $quellverzeichnis/*.desktop; do										# für jede der im Quellverzeichnis vorhandenen .desktop-Dateien... (for each file present in source directory...)
  if [ -f "$dateiname" ]; then	
	dateikennung="$(echo "$dateiname" | rev | cut -d/ -f1 | cut -d. -f2- | rev)"
echo $"Verarbeite Datei:"" $dateikennung"
	anzahl_zeilen="$(sed -n "$=" "$dateiname")"
	while read zeile <&3; do															# ...jeden Eintrag der master-string-Datei abarbeiten (...process each entry in master string file)
		if [ "${zeile::1}" == '"' ]; then
			desktopdatei="$(echo -n "${zeile:1:-1}" | cut -d'|' -f1)"					# Eintragsziel extrahieren (extract target of this entry from its string identifier)
			if [ "$desktopdatei" == "$dateikennung" ]; then 							# Falls Eintragsziel mit aktueller desktop-Datei übereinstimmt, Eintrag verarbeiten (In case target meets recent desktop file, process this entry)
				abschnitt="$(echo -n "$zeile" | cut -d'|' -f2)"							# Zielabschnitt, Eintragstyp und Originalzeichenkette auslesen (read target section within file, type of entry and original string)
				eintragstyp="$(echo -n "$zeile" | cut -d'|' -f3 | cut -d'=' -f1)"
				eintragstyp="${eintragstyp:0:-2}"
				originaleintrag="$(echo -n "$zeile" | cut -d'=' -f2)"
				originaleintrag="${originaleintrag:2:-2}"
				while read -r sprache <&4; do											# für jede Sprache in der Sprachenliste... (for each language in language list file...)
					if [ -e "$temporaerverzeichnis/$ausgabeverzeichnis/$ausgabe_name.$sprache.$ausgabe_erweiterung" ]; then		# ...prüfen, ob eine Übersetzungsdatei existiert (check whether there exists a translation string file)
						uebersetzung="$(grep "$(echo "$zeile" | cut -d= -f1)" "$temporaerverzeichnis/$ausgabeverzeichnis/$ausgabe_name.$sprache.$ausgabe_erweiterung" | cut -d= -f2)"  # Übersetzungstext auslesen (read translation entry from file)
						uebersetzung="$(echo -n ${uebersetzung:2:-2})"
						if [ -n "$uebersetzung" ] && [ "$uebersetzung" != "$originaleintrag" ]; then	# Wenn der Übersetzungseintrag nicht leer ist und nicht identisch mit dem unübersetztnen Originaleintrag... (If translation string is not empty and also not identical to untranslated string)
							n=1
							desktopabschnitt=""
							ist_eingetragen=false
							while read desktopzeile <&5; do										# Lies die Desktopdatei Zeile für Zeile: Übersetzungseintrag in aktuelle Desktopdatei schreiben. (Read desktop file line by line: Write translations into this file)
								if echo "$desktopzeile" | grep "^\[" > /dev/null; then
									if ! $ist_eingetragen && [ -n "$desktopabschnitt" ]; then	# Wenn der Abschnitt keine weiteren Zeilen hat, und die Übersetzung noch nicht eingetragen wurde, füge sie hinzu. (If section doesn't have additional lines, and the language specific entry was not found, add it now)
									  if [ "$desktopabschnitt" == "$abschnitt" ]; then
										m=$((n-1))
											while [ -z "$(sed -n "${m}p" "$dateiname")" ]; do	# Finde die letzte nichtleere Zeile... (find last nonempty line of this section...)
												let $((m--))
											done
											sed -i "${m}a $eintragstyp[$sprache]=$uebersetzung" "$dateiname" # ...und füge den neuen Eintrag dahinter ein (...and add new language entry staight behind it.)
										ist_eingetragen=true
									  fi
									fi
									desktopabschnitt="$(echo "$desktopzeile" | sed 's/\[\(..*\)\]/\1/')"		# Einlesen des nächsten Abschnittsnamens (Read next section name)
								elif echo "$desktopzeile" | grep "^$eintragstyp\[$sprache\]" > /dev/null; then
									if [ "$desktopabschnitt" == "$abschnitt" ]; then							# Wenn der Eintrag im richtige Abschnittsnamen vorhanden ist, ersetze ihn durch die Übersetzung. (If existing language specific entry was found in the correct section, replace it.)
										sed -i "${n}c $eintragstyp[$sprache]=$uebersetzung" "$dateiname"
										ist_eingetragen=true
									fi
								elif [ $n -eq $anzahl_zeilen ] && ! $ist_eingetragen; then
									sed -i "${n}a $eintragstyp[$sprache]=$uebersetzung" "$dateiname"			# Wenn der Eintrag bis zum Dateiende nicht gefunden wurde, füge die Übersetzung hinzu; es gab dann keine weiteren Abschnitte vor deren Beginn sie eingefügt werden konnte. (If no entry was found until EOF, add the entry anyway. There have not been any sections used in this file.)
									ist_eingetragen=true
								fi
								let $((n++))					
							done 5< "$dateiname"
						fi
					fi
				done 4< "$arbeitsverzeichnis/$sprachenliste"
			fi
		fi
	done 3< "$temporaerverzeichnis/$ausgabedatei"
  fi
done

}

# Teil 3: Automatische Übersetzung von Ressourcen des Typs "string" auf Transifex
# (Part 3: Automatic translation of string resources present at transifex)
function translate {
echo "not implemented yet"
# fetch language files (.string type) from transifex, one by one
# convert from apple file format to linux file format
# extract translatable strings which are not translated yet
# send string by string to translation service provider
# add the translations into the file
# convert from linux file format to apple file format
# send the completed translation back to transifex
}

# program starts here
if [ "$1" == "extract" ]; then
	extract
elif [ "$1" == "merge" ]; then
	merge
elif [ "$1" == "translate" ]; then
	translate
else
	cleanup
	exit 1
fi

cleanup
exit 0


