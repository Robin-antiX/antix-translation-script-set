#!/bin/bash

# License: GPL v.3
# Written  (C) 2021 by Robin (for antiX community)
# Set of tiny scripts to (semi-)automatically translate our html documents (e.g. user manuals) automatically.
# consists of "text_vorbereiten.sh" and "text_übersetzen.sh"
# Needs »translate-shell« installed.
# Needs »wkhtmltopdf« installed to create pdf files (in case you decide to activate the respective lines in this script).

# This script set was written in German language originally. I've now translated workflow instructions and most comments to English language to make it easier
# for native English speakers to understand.

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=text_übersetzen.sh

# Workflow (Step-by-step instructions):

# 1.) Validate your html or xhtml against w3.org/validator. Make sure there are (mostly) no errors left. This is important for transifex upload. 
# 2.) Upload your master file (whatever language, doesn't need to be English) to antiX-contribs at transifex, which features handling of resource files in any language.
# 3.) Download the file "FOR TRANSLATION" (in whichever language, but it has to be EMPTY, no existing translations filled in for this language). This will provide you
#	  with a master translation file containing all source strings in original language which is uploadable to transifex again after processing has finished.
# 4.) Run the script »text_vorbereiten.sh« on this downloaded file. This will duplicate the file reformating it to an machine translatable form. Its filename
#	  will be prefixed by the word "BEARBEITET" The new file will have any translatable text on separate lines NOT starting with an html tag.
# 5.) Check the resulting file  BEARBEITET_<something>.(x)html  carefully in this way, that you make sure all text you want to have translated sits on its own line.
#	  Don't touch any of the existing html tags! This would prevent the file from uploading at transifex properly.
#	  If you want to have some Expressions NOT translated either put the untranslatable part of the line at the end of the line before if possible, or add additional
#	  line breaks before and after the untranslatable part and put an html comment tag <!-- --> at the beginning of the line not to be translated.
#		Example:
#			</em></p><p data-tx-text="c089a965fd4d46bd9811f5d78812bd45_se">
#			The user password of user
#			<em>demo</em>
#			is "
#			<em>demo</em>"
#			(no quotes)
#			<br data-tx-tail="360b1a55b99711686e3153cd6877d74b_se"/>
#			The password of
#			<em>root</em>
#			is "
#			<em>root</em>"
#			(no quotes)
#			<br/></p><ul><li data-tx-text="330abf03509da4d15be9106e98cae93e_se"><a href="#help"> 
#			F1 Help System
#			</a></li><li data-tx-text="511abf42fca5ae8a118f37610b27d32f_se"><a href="#language">
#			F2 Language Selection
#		Firstly you don't want have the qoutation mark at the end of the ›is‹ not translated, it might produce unpredictable results on machine translating.
#		So simple put it to a new line and put the comment tag at the line start:
#			is
#			<!-- --> "
#			is "
#			<!-- --> "
#		The same goes for the line
#			F1 Help System
#			F2 Language Selection
#		Who knows what the automatic translators will make of the phrase F1: Formula 1, racing sport or whatever? They don't know we mean a button.
#		So let's simply put the F1 at the End of the (untranslatable) line before.
#			<br/></p><ul><li data-tx-text="330abf03509da4d15be9106e98cae93e_se"><a href="#help">F1 
#			Help System
#			</a></li><li data-tx-text="511abf42fca5ae8a118f37610b27d32f_se"><a href="#language">F2 
#			Language Selection
#		And the lines
#			</em>
#			... Disable video card detection
#			<br data-tx-tail="4b9525f3e3ded523b509ec1659bf2ec9_se"/><em>
#			vcard=menu
#			</em>
#			... Show video card options
#			<br data-tx-tail="c7335dc51bedc08a2b8e0218e34934c3_se"/><em>
#			conwidth=off
#			</em>
#		should get modified to
#			</em>... 
#			Disable video card detection
#			<br data-tx-tail="4b9525f3e3ded523b509ec1659bf2ec9_se"/><em>vcard=menu</em>... 
#			Show video card options
#			<br data-tx-tail="c7335dc51bedc08a2b8e0218e34934c3_se"/><em>conwidth=off</em>
#		since the online translator may swallow the dots.
#	  Check your file carefully for this kind of pitfalls. The master file preparation is the most time consuming part of the process.
#	  Maybe we find a way this step could get automatised for some standard cases as single dots, commas etc. on a translation line which don't need translation.
# 7.) create a list (text file) named "sprachen.lst" containing all the language identifiers you want your file have translated to. One entry per line, starting
#	  always at the beginning of a line An # sign in in front of the respective language will exclude it from being processed, so you can have your default list
#	  containing all langages, excluding languages as needed per case. e.g.:
#			as
#			#sq
#			fil
#			ar
#			zh_TW
#			#zh_CN
#			it
#	No blanks in front of the entries allowed.
# 8.) Apply the text_übersetzen.sh script on this prepared (x)html file. It will separate the untranslatable lines (containing all the html tags) and online
#	  translate the rest of it line by line and put it together again afterwards. The resulting files, complemented by their language identifier will get stored in the
#	  freshly created subfolder ./html
#	  Let the script run unattended until it is done with all translations. If your internet connection breaks for some time it will gracefully pause the
#	  processing until connection is back and resume automatically.
#	  In case you activate the lines for pdf file creation the resulting additional files will get stored within a freshly created subfolder ./pdf  — Please Check
#	  whether the settings in the respective script line for pdf creation meet your needs (eg. paper format or borders; instead of  --page-height 210 --page-width 210
#	  you could use  -s A4  or  -s Letter )
# 9.) Upload all the files found in the folder ./html to transifex.
# 10.) Download all translations in file format »for use« from transifex.

# The scripts are expected to sit in the same directory as the downloaded html master file to be processed. Make sure there is no subdirectory named ./html in it!
# WARNING: No checks are done, any files with identical names as the processed and the subdirectories ./html ./pdf will get overwritten without asking.
# Make sure to have backup copies at hand elsewhere in case processing fails.


# ! PLEASE USE THE TEST AREA "antix-contribs2" for any uploading attempts until		!
# ! you are absolutely sure the translation files generated automatically by this   !
# ! script set will not mess up any existing translations on transifex.				!


#----------------------- configurable settings -------------------------
sprachenliste="./sprachen.lst"					# List of languages te get processed. Put into the file referred to here all the language identifiers (two character, e.g.
												# fr or en; some four character ids will work also, like zh-TW, but be aware this is NOT an underline in between here, as
												# gettext would expect.), one per line the html file should get translated to. Comment languages out with # is obeyed.
htmldatei="for_translation_antix-contribs_antix-bootscreen-helpmenu.xhtml"		# name of the html file to translate. Use the same name as used while applying the first
												# part of the script set text_vorbereiten.sh  
ausgangssprache="en"							# source language (two letter language identifer, or some four sign. character ids will work also, but keep in mind to use
												# - instead of _ as separator)
basis_ausgabedateiname="$htmldatei"				# Base filename for output. Default is  ="$htmldatei" — If you want a different outfile name, enter here an explicit
												# base filename, the language identifier of the target language will be added automatically between filename and extension.
zielverzeichnis="./fertig"						# set the target directory where to put the outfiles. "." for current dir is also ok.
#-----------------------------------------------------------------------

mkdir -p "$zielverzeichnis/html"
temporaerdatei_01="/tmp/text_übersetzen-$$01.tmp"
trap $([ -e "$temporaerdatei_01" ] && rm -f "$temporaerdatei_01") EXIT

arbeitsdatei="./BEARBEITET_""$htmldatei"
echo "" >> $arbeitsdatei		# Leerzeile am Dateiende anfügen, für den Fall daß keine vorhanden ist. (fehlt sie, wird die letzte Zeile nicht verarbeitet)

while read -r zielsprache <&3; do
	if ! echo "$zielsprache" | grep -E '^#' > /dev/null; then
		echo $"Übersetze zu"" ""$zielsprache"
		ausgabedatei="$(echo "$basis_ausgabedateiname" | rev | sed -e "s/\(^..*\)\./\1.$(echo $zielsprache | rev)./" | rev)"
>"$zielverzeichnis/html/$ausgabedatei"
		while read -er zeile; do
			# echo -n "."
			if ! echo "$zeile" | grep -E "^<" >/dev/null; then
				while ! nc -zw1 8.8.8.8 443; do
					sleep 30			# wait gracefully in case internet connection is broken for some time
				done
				trans -s "$ausgangssprache" -t "$zielsprache" -b --show-original n --show-languages n --show-original-dictionary n \
--show-dictionary n --show-alternatives n --show-prompt-message n --show-original-phonetics n --show-translation-phonetics n \
--show-translation y -- "$zeile" >> "$zielverzeichnis/html/$ausgabedatei"
				tail -n 1 "$zielverzeichnis/html/$ausgabedatei"			# activate this to see what translation is written into the file recently.
				sleep $(($(echo ${RANDOM:0-1} | sed 's/^ /0/')*11+21))  # use this to prevent the translation service from blocking your IP for
																		# presumed flooding attrack. Value range generated is ~20 sec. to ~2 min.
				# sleep $(($(echo ${RANDOM:0-2} | sed 's/^0//')*2+31))  # other formula, will generate values between ~30 seconds and ~4,5 minutes
																		# in case formula above doesn't prevent you from being blocked.
				# sleep $(($(echo ${RANDOM:0-1} | sed 's/^ /0/')*2+5))  # fast formula in case your IP isnt not blocked generating values
																		# between 5 sec. and ~20 sec.
				# sleep $(($(echo ${RANDOM:0-1} | sed 's/^ /0/')+1))    # super fast formula generating values between 1 sec. and 10 sec.
			else
				echo "."
				echo "$zeile" >> "$zielverzeichnis/html/$ausgabedatei"
			fi
		done < "$arbeitsdatei"
		echo ""
		tr "\n" " " < "$zielverzeichnis/html/$ausgabedatei" > "$temporaerdatei_01"
		mv "$temporaerdatei_01" "$zielverzeichnis/html/$ausgabedatei"
		sed -i 's/<!-- -->//g' "$zielverzeichnis/html/$ausgabedatei"

#++++++++++++++ activate this section for processing pdf file output +++++++++++++
#		mkdir -p "$zielverzeichnis/pdf"
#		pdfdatei="$(echo "$(echo "$ausgabedatei" | rev | cut -d. -f 2- | rev).pdf")"
#		wkhtmltopdf -g -O Portrait --page-height 210 --page-width 210 -d 72 -B 20 -L 20 -R 20 -T 20 "$zielverzeichnis/html/$ausgabedatei" "$zielverzeichnis/pdf/$pdfdatei"
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		sed -i "s/^"$zielsprache"$/#"$zielsprache"/" "$sprachenliste"		# needed to resume with next language not already done after any interuption of processing.
		zeitkonstante=$(($(echo ${RANDOM:0-1} | sed 's/^ /0/')*10+31))
		echo $"Übersetzung zu ""$zielsprache "$"abgeschlossen. Fortsetung in ""$zeitkonstante "$"Sekunden. Taste »q« zum Beenden."
		while :; do
			read -rsn1 -t $zeitkonstante entscheidung	# wait some additional seconds befor connecting online translation service again while asking
														# user to stop processing.
			if [ "$entscheidung" = "q" ] || [ "$entscheidung" = "Q" ]; then
				echo -e $"Die Übersetzung wurde unterbrochen.\nZum Fortsetzen mit der nächsten Sprache Script erneut starten."
				exit 0
			elif [ "$entscheidung" = "" ]; then
				break
			fi
		done

	fi
done 3< "$sprachenliste"

exit 0


