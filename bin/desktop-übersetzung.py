#!/usr/bin/python3
# -*- coding: utf-8 -*- vim:fenc=utf-8:ft=python:et:sw=4:ts=4:sts=4

# Name: desktop-übersetzung
# Version: 1.1-Test-56
# Author: Robin.antiX
# 2022, 2023, 2024
# License: GPLv3
# Parser subfunctions by Esa Määttä (pyxdg-open, GPL v.3)

# antiX .desktop file strings translation update harvester for transifex.

# - Needs new transifex client (https://github.com/transifex/cli/releases/latest)
#   installed and its root config file present (~/transifexrc). Version 1.6.5
# - Needs git console client to be installed (Version 2.39.2).
# - Needs translate-shell >= 0.9.6.12 console translator to be installed.

# This script was originaly written in German language for internal usage.
# To use it localised to English (or other languages) please download the
# respective language file (.mo type) from gitlab (...) and copy it/them
# to the system-folder /usr/share/locale/<LanguageID>/LC_MESSAGES before
# running the script. Then all its output messages on console will come
# up localised to English or other languages.
#
# Purpose and usage:
#
# Management of .desktop files of antiX releases.
#
# Call script from within an empty translation base folder. Use options
# according to -h or --help informational output. Needs a language list
# file present within base folder. (List of Language IDs in Unix style (either
# 2 or 3 character ID or xx(x)_YY(Y) e.g. „pt_BR” or „sv”.),  one entry per
# line, comment lines (#) allowed, but no inline comments. Script will
# create project subdirectory from command line filed tx project-„slug” if
# not present. Creates resource subdirectory within project subdirectory
# if not present. Within the resource directory is expected to find a folder
# named „applications” containing a copy of the original .desktop files from
# which the transifex strings files have been created or are to be created.
# An antix subdirecory within the applications directory is recognised.
# Either populate the application directory and its antix subdirectory
# once the structure was created by the script, or supply it
# beforehand. On each run a fresh dated folder will be created on execution,
# taking all processed files. Depending on the working mode (Extract, Update,
# Source update, Translate and Backup), three subfolders will be created
# and populated automatically within the target subfolder, taking all
# results of the extracting, harvesting, source-updating or translating run.
#
# – In extract mode -e the script extracts all translatable strings from all
# .desktop files provided in the applications subfolder of the resource.
# The extracted strings are collated to language strings file and uploaded
# to transifex for translation.
#
# – In update mode -a script fetches all translations string files from
# transifex servers and updates the .desktop files found in applications
# folder to the languages set in language list file. The updated .desktop
# files will get stored in the target folder
#    ./<project>/<resource>/<date>/transifex/applications   while original
# .desktop files in ./<project>/<resource>/applications stay untouched.
# Use -g for git upload preparation after processing. This option is only
# available in update mode. If -HL is filed additionally, the updated
# resource will get uploaded to gitlab.
#
# – In source update mode -q from the desktop files provided in the appli-
# cations subfolder all translatable strings will get extracted exactly as
# in the extraction mode, but before writing them to the new strings files,
# the translations will be updated from the recent translation state down-
# loaded from transifex for each language present in language list file.
# After this the new .desktop files are written. If -HL is filed additionally,
# the updated resource will get uploaded back to transifex.
#
# - In translation mode -ü for each language the translation file will be
# downloaded separately, one by one, and all empty strings sent via trans
# console client to an online translations serveice provider. The returned
# translations are stored to the recently processed strings language file.
# Translations present at transifex before
# the translation run has started for a specific language will not get
# overwritten, but skipped and preserved. If -HL is filed additionally,
# each translated language file will get uploaded back to transifex
# immediately after translation to the very language was completed.

# – In backup mode -s the current state of translations (independend of the
# translation list file) from transifex will get locally backuped along
# with the source strings file and the locally provided .desktop files
# from the resource's applications subfolder.
#
# Use -lq option to use local strings files provided in a subfolder named
# „local” residing within the resource directory instead of downloading
# them from transifex. This will take effect in update processing mode and
# in translation processing mode only. Can't be used with together with -HL.
#
# Use the -g option to prepare all results for upload to gitlab. Only
# available in -a processing mode. Expects a URL pointing to the respective
# gitlab resource, e.g. https://gitlab.com/Robin-antiX/antix23-desktop-files.
# To actually upload the files to gitlab use the -HL option additionally.
#
# Use -HL flag for transferring all results to the transifex and gitlab
# servers instead of merely storing them prepared for upload within
# the respective subfolders of the recent dated run folder. Transfer to
# gitlab will only take effect when -g flag is set also.

# ----------------------------------------------------------------------
# Updates and fixes:
# ----------------------------------------------------------------------
# Fixed: If antix subfolder in applications is empty, it will no longer be
#   written into the package, while its content is still processed if present.
#   (so it can be used for processing antiX versions having this folder still)
# Update: in its update mode (-a) the script will fetch the all the .desktop
#   files to be processed from the given gitlab space /originale/applications
#   folder rather than using the local copies. Same goes for the languages
#   list file sprachen.lst which resides in the root folder of the git
#   ressource. No longer any need to provide them locally in this mode.
# Update: Added md5sum list creation to packaging.
# Update: Added alternative packgaing (many thanks abc-nix!) using debuild
#   This requires additionally the packages build-essentials, debhelper,
#   dev-scripts etc. This alternative method can currently be activated by
#   uncommenting the respective lines, and commenting out the dpkg-deb related
#   package preparations.
# Update: Changed package layout back to original design, storing .desktop files
#   in /usr/local/lib/antiX/‹resource›/antix23-desktop-files/reservoir folder
#   and applying dpkg-diversion selectively afterwards. Requires an additional
#   apt-hook now to dpkg-divert the .desktop files from later installed packages,
#   preferably placed in desktop-menu, so the check is performed each time
#   the antiX menu is refreshed.

import re
from collections import OrderedDict
import io
import gettext
import os
import shutil
from datetime import datetime
import subprocess
import sys
import argparse
from time import sleep
from random import randrange
import hashlib
import gzip

version = '1.1-Test-56'

_ = gettext.gettext
gettext.bindtextdomain("desktop-übersetzung.py","/usr/share/locale")
gettext.textdomain("desktop-übersetzung.py")

def Stringsdateien_lesen():
# Liest alle übersetzten Einträge aus .strings-Dateien in ein Wörterbuch (reads all localised strings files entries to strings dictionary)
    global Haupt_Wörterbuch
    global Eintragszähler
    r = re.compile(r'^\"(.+)\|(.+)\|(.+)\" = \"(.*)\";$')
    for Sprache in Sprachliste:
        if Haupt_Wörterbuch.get(Sprache):
            Sprach_Wörterbuch = Haupt_Wörterbuch.get(Sprache)
        else:
            Sprach_Wörterbuch = {}
        if Sprache == Projektsprache:
            tx_Stringsdatei = "{0}/{1}.strings".format(Txpfad, Ressourcenkennung)
        else:
            tx_Stringsdatei = "{0}/{1}.{2}.strings".format(Stringspfad, Ressourcenkennung, Sprache)
        try:
            #with open(tx_Stringsdatei, "r", encoding="utf-16") as str_Datei:  # works also, but is ressource hungry and time consuming, so let's decode later.
            with open(tx_Stringsdatei, "rb") as str_Datei:
                Einträge = str_Datei.read()
        except:
            print(Farbe['Rot_Fettdruck'] + _("String-Datei für {0} nicht vorhanden.").format(Sprache) + Farbe['Weiß_Normaldruck'])
            continue
        Einträge = io.StringIO(str(Einträge.decode('utf-16')))
        for Zeile in Einträge:
            try:
                Eintrag = re.findall(r, Zeile)[0]
            except:
                continue
            if not Eintrag[3] == "":
                Eintragszähler += 1
                if not Sprach_Wörterbuch.get(Eintrag[0]):
                    Sprach_Wörterbuch.update({Eintrag[0] : {}})
                if not Sprach_Wörterbuch[Eintrag[0]].get(Eintrag[1]):
                    Sprach_Wörterbuch[Eintrag[0]].update({Eintrag[1] : {}})
                Sprach_Wörterbuch[Eintrag[0]][Eintrag[1]].update({Eintrag[2] : Eintrag[3]})
        Haupt_Wörterbuch.update({Sprache : Sprach_Wörterbuch})
    return Eintragszähler

def Stringsdateien_schreiben():
    Hinweise = {
        'Name' : 'This entry refers to the „first name” of the program, as it will be displayed by default e.g. in menus. Keep it as \
short as possible, and please avoid descriptive additions like superordinates or category terms. (Examples: Libreoffice, Claws Mail, Geany ...)',
        'GenericName' : 'This entry refers to the „surname” of the program, it may be displayed e.g. in menus, additionally or as a \
replacement for the name, in case this feature is activated. Please keep it as short as possible and consider this term as a \
superordinate (e.g. „Email client” or „Office suite”, „Text editor (commandline)”, „Text editor” „Calculator (scientific)”, \
„Calculator (simple)”.',
        'Comment' : 'This entry may be displayed e.g. as additional component or as a tooltip in menus, if this feature is activated. \
You may explain here the core functionallity of the program or its peculiarity in short words, it is a good idea to use some additional \
synonyms within this entry.',
         'Keywords' : 'Search terms used to find this application. Don’t translate the semicolons! The list MUST also end with a semicolon!'
    }
    for Sprache in Sprachliste:
        if not Optionen.get('Übersetzungsmodus'): print(' · Sprache: ' + Farbe['Weiß_Fettdruck'] + Sprache + Farbe['Weiß_Normaldruck'] if not Sprache == Projektsprache else ' · Quellstrings: ' + Farbe['Weiß_Fettdruck'] + Projektsprache + " ({0})".format(Ressourcensprache) + Farbe['Weiß_Normaldruck'])
        Stringsliste = []
        try:
            Sprach_Wörterbuch = Haupt_Wörterbuch.get(Sprache)
        except:
            continue
        for Ressource in Desktopdateiliste:
            Abschnittsliste = ""
            try:
                Ressourcen_Wörterbuch = Sprach_Wörterbuch.get(Ressource)
                Abschnittsliste = Ressourcen_Wörterbuch.keys()
            except:
                continue
            for Abschnitt in Abschnittsliste:
                Typenliste = ""
                try:
                    Typen_Wörterbuch = Ressourcen_Wörterbuch.get(Abschnitt)
                    Typenliste = Typen_Wörterbuch.keys()
                except:
                    continue
                for Typ in Typenliste:
                    try:
                        Eintrag = Typen_Wörterbuch.get(Typ)
                    except:
                        continue
                    Stringsliste.append("""/* File or program name: . . . . . . . {0}
Section within .desktop file: . . {1}
Entry type: . . . . . . . . . . . . . . . . {2}=
{4} */
"{0}|{1}|{2}" = "{3}";
""".format(Ressource, Abschnitt, Typ, Eintrag, Hinweise.get(Typ)))
        Ausgabestring = '\n'.join(map(str, Stringsliste))
        Ausgabestring = Ausgabestring.encode('utf-16')
        if Sprache == Projektsprache:
            tx_Stringsdatei = "{0}/{1}.strings".format(Txpfad, Ressourcenkennung)
        else:
            tx_Stringsdatei = "{0}/{1}.{2}.strings".format(Stringspfad, Ressourcenkennung, Sprache)
        with open(tx_Stringsdatei, 'wb') as str_Datei:
            str_Datei.write(Ausgabestring)

class Entry(object):
# derived from Esa Määttä's pyxdg-open GPL v.3 desktop file parser.
    def __init__(self, key, locale, value):
        self.key = key
        self.value = value
        self.locale = locale
    def __str__(self):
        if isinstance(self.value, bool):
            value = "true" if self.value else "false"
        elif isinstance(self.value, list):
            value = ";".join(self.value)
        else:
            value = self.value
        if self.locale is not None:
            return "{key}[{locale}]={_value}".format(_value=value, **self.__dict__)
        else:
            return "{key}={_value}".format(_value=value, **self.__dict__)

def parse(Quellstring, Ressource):
# returns desktop file content complemented by translations from string file dictionary.
# original parse function main structure and Entry class based on Esa Määttä's pyxdg-open GPL v.3 desktop file parser.
# adapted for translation purposes by Robin-antiX
    global Haupt_Wörterbuch
    global Eintragszähler
    print(' · ' + Ressource)
    # Zeilen aus Desktopdateien mit Token versehen (tokenize desktop files lines)
    tokens = generate_tokens(Quellstring)
    # Einträge aus Desktop Dateien verarbeiten (process desktop files entry sections)
    entry_sections = {}
    if True: # want to see all error messages
    #try:
        current_sections = None
        entry_sections[str(None)] = [] #Kommentare vor dem ersten Abschnitt mitnehmen (catch comment entries before first section header)
        for t in tokens:
            name = t[0]    #string
            values = t[1]  #tuple
            # Leere Zeilen beibehalten wegen besserer Lesbarkeit (keep empty lines (for sake of readability))
            if name == "EMPTY_LINE":
                entry = ''
                entry_sections[str(current_sections)].append(entry)  #Kommentare können schon vor dem ersten Abschnitt stehen (comments are allowed before first section.)
                continue
            # Kommentarzeilen 1:1 kopieren (copy comment lines)
            elif name == "COMMENT_LINE":
                entry = '#' + str(values[0])
                entry_sections[str(current_sections)].append(entry)  #Kommentare können schon vor dem ersten Abschnitt stehen (comments are allowed before first section.)
                continue
            # Abschnittsnamen lesen (read section headers)
            elif name == "SECTION_HEADER":
                current_sections = values[0]
                entry_sections[current_sections] = []
                continue
            # Einträge abarbeiten (process entries)
            elif name == "ENTRY":
                entry_name = values[0]
                entry_value = values[2]
                entry_locale = values[1].strip("[]") if values[1] else None
                entry = Entry(entry_name, entry_locale, entry_value)
                # Check boolean entries
                if entry.key in ["NoDisplay", "Hidden", "Terminal", "StartupNotify", "X-MultipleArgs"]:
                    if not entry.value in ["0", "1", "false", "true", "False", "True"]:
                        print(_("Fehler: Der Schlüssel „{0}” enthielt keinen Wert des Datentyps „bool”!").format(entry.key))
                    elif entry.value in ["0", "false", "False"]:
                        entry.value = False
                    elif entry.value == ["1", "true", "True"]:
                        entry.value = True
                # Check multiple string entries (string lists)
                elif entry.key in ["OnlyShowIn", "NotShowIn", "Actions", "MimeType", "Categories"]:
                    # TODO: ";" can be escaped with '\' which can of course(?)
                    # be also escaped so splitting should only be done if ';' is
                    # not prefixed with any number of '\' chars or prefixed with
                    # mod 2 number of '\' chars. So <count of '\'> % 2 == 0 ==>
                    # split.
                    entry.value = entry.value.split(";")
                entry_sections[current_sections].append(entry)
                # Übersetzungen aus dem Hauptwörterbuch hinzufügen (Add translations from strings file dictionary)
                if entry_name in ['Name', 'GenericName', 'Comment', "Keywords"]:
                    if Optionen.get('Aktualisierungsmodus'):
                        for Sprache in Sprachliste:
                            #entry_value = Haupt_Wörterbuch.get(Sprache + '|' + Ressource + '|' + current_sections + '|' + entry_name)
                            try:
                                entry_value = Haupt_Wörterbuch[Sprache][Ressource][current_sections][entry_name]
                            except:
                                continue
                            if not entry_value == values[2]: # skip entries 1:1 copied from source
                                entry_locale = Sprache
                                entry = Entry(entry_name, entry_locale, entry_value)
                                entry_sections[current_sections].append(entry)
                    elif Optionen.get('Extraktionsmodus') or Optionen.get('Quellaktualisierungsmodus'):
                        if not entry_locale:
                            Sprache = Projektsprache
                        else:
                            Sprache = entry_locale
                        #Schlüssel = Sprache + '|' + Ressource + '|' + current_sections + '|' + entry_name
                        #Haupt_Wörterbuch.update({Schlüssel : entry_value})
                        if not Haupt_Wörterbuch.get(Sprache):
                            Haupt_Wörterbuch.update({Sprache : {}})
                        if not Haupt_Wörterbuch[Sprache].get(Ressource):
                            Haupt_Wörterbuch[Sprache].update({Ressource : {}})
                        if not Haupt_Wörterbuch[Sprache][Ressource].get(current_sections):
                            Haupt_Wörterbuch[Sprache][Ressource].update({current_sections : {}})
                        Haupt_Wörterbuch[Sprache][Ressource][current_sections].update({entry_name : entry_value})
                        Eintragszähler += 1
            else:
                assert(False)
    # except SyntaxError as e:
    #    raise e
    output_string = ''
    for sect in entry_sections:
        output_string += ('\n' + "[{0}]\n".format(sect) if sect != str(None) else '') + '\n'.join(map(str, entry_sections[sect]))
    return (output_string + '\n').split("\n", 1)[1]

def generate_tokens(text):
# token generator by Esa Määttä from pyxdg-open GPL v.3 desktop file parser.
# returns tokenised strings from a desktop file read into a string
    reg = r"""(?P<ENTRY>^(.+?)(\[.+?\])?=(.*)$\n?)|(?P<COMMENT_LINE>^#(.*)\n?)|(?P<EMPTY_LINE>^[ \t\r\f\v]*\n)|(?P<SECTION_HEADER>^\[(.+?)\]\s*$\n?)"""
    r = re.compile(reg, re.MULTILINE)
    generate_tokens.sections = OrderedDict(sorted(r.groupindex.items(), key=lambda t: t[1]))
    # Make generate_tokens.sections contain mapping from regex group name to submatch
    # range. Submatch range start-1 is the whole match.
    last_i = None
    for i in generate_tokens.sections.items():
        if last_i == None:
            last_i = i
            continue
        generate_tokens.sections[last_i[0]] = (last_i[1], i[1]-1)
        last_i = i
    generate_tokens.sections[last_i[0]] = (last_i[1], r.groups)
    pos = 0
    while True:
        m = r.match(text, pos)
        if not m:
            if pos != len(text):
                raise SyntaxError(_("Fehler: Die Erzeugung der Token ist fehlgeschlagen!"))
            break
        pos = m.end()
        yield m.lastgroup, m.groups()[ generate_tokens.sections[m.lastgroup][0]:
                generate_tokens.sections[m.lastgroup][1]]

def Hauptwörterbuch_übersetzen():
# Achtung: Alle Ersetzungen sind KEIN normalen Leerzeichen (u0020) sondern jeweils die entsprechenden Unicodezeichen entsprechend der Zeichennummer. (beware: all the replacements are NOT default blanks (u0020) or empty strings, but actually the respective utf space and formatting characters)
    Ersetzungen = {'"' : '”', "\u0027" : '’', 'u2000' : ' ', 'u2001' : ' ', 'u2002' : ' ', 'u2003' : ' ', 'u2004' : ' ',
        'u2005' : ' ', 'u2006' : ' ', 'u2007' : ' ', 'u2008' : ' ', 'u2009' : ' ', 'u200a' : ' ', 'u200b' : '​',
        'u200c' : '‌', 'u200d' : '‍', 'u200e' : '‎', 'u200f' : '‏'}
# r = re.compile(r'\"(.*)\"')
# text = re.sub(r, '“\1”', text)
    for Sprache in Sprachliste:
        if Sprache == Projektsprache: continue
        Stringsliste = []
        #if not Haupt_Wörterbuch.get(Projektsprache): continue
        if not Haupt_Wörterbuch.get(Sprache):
            Haupt_Wörterbuch.update({Sprache : {}})
        for Ressource in Desktopdateiliste:
            if not Haupt_Wörterbuch[Projektsprache].get(Ressource):
                print(Farbe['Rot_Fettdruck'] + _("Ressource {0} hat keinen Eintrag im Hauptwörterbuch").format(Ressource) + Farbe['Weiß_Normaldruck'])
                continue
            if not Haupt_Wörterbuch[Sprache].get(Ressource):
                Haupt_Wörterbuch[Sprache].update({Ressource : {}})
            for Abschnitt in Haupt_Wörterbuch[Projektsprache][Ressource]:
                #if not Haupt_Wörterbuch[Projektsprache][Ressource].get(Abschnitt): continue
                if not Haupt_Wörterbuch[Sprache][Ressource].get(Abschnitt):
                    Haupt_Wörterbuch[Sprache][Ressource].update({Abschnitt : {}})
                for Typ in Haupt_Wörterbuch[Projektsprache][Ressource][Abschnitt]:
                    #if not Haupt_Wörterbuch[Projektsprache][Ressource][Abschnitt].get(Typ): continue
                    if not Haupt_Wörterbuch[Sprache][Ressource][Abschnitt].get(Typ):
                        Quell_Eintrag = Haupt_Wörterbuch[Projektsprache][Ressource][Abschnitt].get(Typ)
                        print(" " + Farbe['Weiß_Fettdruck'] + Quell_Eintrag + Farbe['Weiß_Normaldruck'])
                        Eintrag = Text_Übersetzen(Quell_Eintrag, Ressourcensprache, Sprache, Typ).translate(Ersetzungen)
                        print(" " + Eintrag + '\n')
                        #print("(S=" + Sprache, "R=" + Ressource, "A=" + Abschnitt, "T=" + Typ + ")")
                        Haupt_Wörterbuch[Sprache][Ressource][Abschnitt].update({Typ : Eintrag})
                        sleep(randrange(6)+5)

def Text_Übersetzen(text,ein,aus,eintragstyp):
# Übersetzung der Stringvariable 'text' mittels des Konsolenübersetzers 'trans' (translate 'text' through the trans command line client)
    if not eintragstyp == 'Keywords':
        process = subprocess.Popen(
            ["trans","-s",ein,"-t",aus,"-b","--show-original","n","--show-languages","n","--show-original-dictionary","n","--show-dictionary","n","--show-alternatives","n","--show-prompt-message","n","--show-original-phonetics","n","--show-translation-phonetics","n","--show-translation","y","--no-ansi","--no-browser","--no-view","--"],
            stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding='utf8')
        stdout,stderr = process.communicate(text)      
        if process.returncode == 0: return stdout.rstrip('\n') # return translated to foreign language, remove odd characters left over from zero width blanks added by translation service provider not caught by translate-shell
        raise Exception(stderr)
    else:
        Schlüsselworte = ""
        for Schlüsselwort in text.strip(';').split(';'):
            process = subprocess.Popen(
                ["trans","-s",ein,"-t",aus,"-b","--show-original","n","--show-languages","n","--show-original-dictionary","n","--show-dictionary","n","--show-alternatives","n","--show-prompt-message","n","--show-original-phonetics","n","--show-translation-phonetics","n","--show-translation","y","--no-ansi","--no-browser","--no-view","--"],
                stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding='utf8')
            stdout,stderr = process.communicate(Schlüsselwort)      
            if process.returncode == 0:
                Schlüsselworte += stdout.rstrip('\n') + ";" # cumulate translated to foreign language, remove odd characters left over from zero width blanks added by translation service provider not caught by translate-shell
            else:
                raise Exception(stderr)
        return Schlüsselworte

def Desktopdatei_lesen(current_desktop_file):
# Liest eine Desktopdatei ein und entfernt dabei die alten Übersetzungseinträge (returns desktop file as list of strings cleaned from translations)
    try:
        with open(Desktop_Quellpfad + "/antix" + current_desktop_file, "r") as desk_Datei:   # Zusatz für antix Unterverzeichnis (amendment for antix subfolder)
            desk_lst = desk_Datei.read().splitlines()
    except FileNotFoundError:
        with open(Desktop_Quellpfad + current_desktop_file, "r") as desk_Datei:
            desk_lst = desk_Datei.read().splitlines()
    if Optionen.get('Extraktionsmodus') or Optionen.get('Quellaktualisierungsmodus'):
        return desk_lst
    else:
        rc = re.compile(r'^Name\[|^GenericName\[|^Comment\[|^Keywords\[')
        return [x for x in desk_lst if not rc.match(x)]

def Desktopdatei_schreiben(current_desktop_file, content):
# Schreibt die übersetzte Desktopdatei ins Zielverzeichnis (writes desktop file from string, overwrite if exists.)
    if os.path.isfile(Desktop_Quellpfad + "/antix" + current_desktop_file):   # Zusatz für antix Unterverzeichnis (amendment for antix subfolder)
        with open(Desktop_Zielpfad + "/antix" + current_desktop_file, "w") as desk_Datei:
            desk_Datei.write(content)
    else:
        with open(Desktop_Zielpfad + current_desktop_file, "w") as desk_Datei:
            desk_Datei.write(content)

def Voraussetzungen_01():
# (preliminary Checks)
# Fehlende Ordnerstrukturen erzeugen, sicherstellen, daß die Zielpfade leer und die Quelldateien vorhanden sind. (create missing folders and make sure the outpath is not populated and check the expected desktop infiles are present)
    Sicherungsanhang = " ({0})".format(datetime.now().strftime("%d.%m.%Y-%H:%M:%S"))
    if os.path.isdir(Desktop_Zielpfad) or os.path.isdir(Stringspfad):
        if len([x for x in (next(os.walk(Desktop_Zielpfad), (None, None, []))[2])]) != 0 or len([x for x in (next(os.walk(Stringspfad), (None, None, []))[2])]) != 0:
            # Position des Eintrags: Befehlszeile, Information wenn das Zielverzeichnis nicht leer ist und eine Sicherungskopie erzeugt wird. Substitution: {0} = Verzeichnisname (String-Position: Command line info message if out-folder was not empty and a backup was created. Substitution: {0} = foldername)
            print(Farbe['Weiß_Fettdruck'] + _("Ausgabeverzeichnis nicht leer. Erzeuge Sicherungskopie:") + Farbe['Weiß_Normaldruck'] + '\n  ' + Txpfad + Sicherungsanhang)
            os.rename(Txpfad, Txpfad + Sicherungsanhang)
            os.makedirs(Stringspfad, exist_ok=True)
    if not os.path.isdir(Desktop_Zielpfad):
        # Position des Eintrags: Befehlszeile, Information wenn das Zielverzeichnis neu erstellt werden muß. Substitution: {0} = Verzeichnisname (String-Position: Command line info message if out-folder was to be created. Substitution: {0} = foldername)
        print(Farbe['Weiß_Fettdruck'] + _("Erzeuge Augabeverzeichnis:") + Farbe['Weiß_Normaldruck'] + '\n  ' + Desktop_Zielpfad)
        os.makedirs(Desktop_Zielpfad, exist_ok=True)
        os.makedirs(Desktop_Zielpfad + '/antix', exist_ok=True)
    if not Optionen.get('Extraktionsmodus') and not os.path.isdir(Paket_Zielpfad):
        # Position des Eintrags: Befehlszeile, Information wenn das Zielverzeichnis neu erstellt werden muß. Substitution: {0} = Verzeichnisname (String-Position: Command line info message if out-folder was to be created. Substitution: {0} = foldername)
        print(Farbe['Weiß_Fettdruck'] + _("Erzeuge Paketverzeichnis:") + Farbe['Weiß_Normaldruck'] + '\n  ' + Paket_Zielpfad)
        os.makedirs(Paket_Zielpfad, exist_ok=True)
    if (Optionen.get('Extraktionsmodus') or Optionen.get('Aktualisierungsmodus')) and not os.path.isdir(Stringspfad):
        # Position des Eintrags: Befehlszeile, Information wenn das Zielverzeichnis neu erstellt werden muß. Substitution: {0} = Verzeichnisname (String-Position: Command line info message if out-folder was to be created. Substitution: {0} = foldername)
        print(Farbe['Weiß_Fettdruck'] + _("Erzeuge Augabeverzeichnis:") + Farbe['Weiß_Normaldruck'] + '\n  ' + Stringspfad)
        os.makedirs(Stringspfad, exist_ok=True)
    if (Optionen.get('Aktualisierungsmodus') and Optionen.get('Gitlab')) and not os.path.isdir(Gitpfad):
        # Position des Eintrags: Befehlszeile, Information wenn das Zielverzeichnis neu erstellt werden muß. Substitution: {0} = Verzeichnisname (String-Position: Command line info message if out-folder was to be created. Substitution: {0} = foldername)
        print(Farbe['Weiß_Fettdruck'] + _("Erzeuge Augabeverzeichnis:") + Farbe['Weiß_Normaldruck'] + '\n  ' + Gitpfad)
        os.makedirs(Gitpfad, exist_ok=True)

def Voraussetzungen_02():
# (preliminary Checks)
    # Prüfen, ob die strings-Dateien vorhanden sind (check the expected strings files are present)
    if len([x for x in (next(os.walk(Stringspfad), (None, None, []))[2]) if x.endswith('.strings')]) == 0:
        # Position des Eintrags: Befehlszeile, Fehlermeldung wenn keine strings-Dateien gefunden wurden. Substitution: {0} = Verzeichnisname (String-Position: Command line error message if no strings files were found. Substitution: {0} = foldername)
        print(Farbe['Rot_Fettdruck'] + _("Fehler: Im Verzeichnis {0} wurden keine .strings-Dateien gefunden.").format(Farbe['Rot_Normaldruck'] + '\n  ' + Stringspfad + '\n' + Farbe['Rot_Fettdruck']) + Farbe['Weiß_Normaldruck'])
        exit(1)
    #Todo: check whether all requestesd languages in locale file have representatives whithin the strings folder

def Voraussetzungen_03():
# (preliminary Checks)
    # Prüfen, ob die Sprachdatei vorhanden ist, entweder von Gitlab oder lokal. (check whether processing language list file is present, either from gitlab or locally provided)
    if not os.path.isfile(Sprachdatei):
        # Position des Eintrags: Befehlszeile, Fehlermeldung wenn keine Datei mit der Sprachenliste gefunden wurde. Substitution: {0} = Dateiname (String-Position: Command line error message if no language list file was found. Substitution: {0} = filename)
        print(Farbe['Rot_Fettdruck'] + _("Fehler: Die Sprachdatei {0} wurde nicht gefunden.").format('\n  ' + Farbe['Rot_Normaldruck'] + Sprachdatei + Farbe['Rot_Fettdruck'] + '\n') + Farbe['Weiß_Normaldruck'])
        exit(1)
    # Prüfen, ob die .desktop-Dateien von gitlab korrket übertragen wurden und vorhanden sind (check the expected .desktop files either from gitlab or locally are present)
    if len(Desktopdateiliste) == 0:
        # Position des Eintrags: Befehlszeile, Fehlermeldung wenn keine Desktopdateien gefunden wurden. Substitution: {0} = Verzeichnisname (String-Position: Command line error message if no desktop files were found. Substitution: {0} = foldername)
        print(Farbe['Rot_Fettdruck'] +  _("Fehler: Im Verzeichnis {0} wurden keine .desktop Dateien gefunden").format('\n  ' + Farbe['Rot_Normaldruck'] + Desktop_Quellpfad + Farbe['Rot_Fettdruck'] + '\n') + Farbe['Weiß_Normaldruck'])
        if not os.path.isdir(Desktop_Quellpfad):
            os.makedirs(Desktop_Quellpfad, exist_ok=True)
        exit(1)

def Sprachdatei_lesen():
# Sprachen, die in der Sprachdatei definiert sind, in ein globales Tupel einlesen. (read languages from language file into languages tuple)
    global Sprachliste
    with open(Sprachdatei, "r") as S_Datei:
        Sprachliste = S_Datei.read().splitlines()
    Sprachliste = [x for x in Sprachliste if not (x.startswith('#') or len(str.strip(x)) == 0)]

def Transifex_Initiieren():
# Schreibe ressourcenspezifische .tx/config Datei (write .tx/config resource specifically)
    os.makedirs(tx_Verzeichnis, exist_ok=True)
    tx_config = """[main]
host = https://www.transifex.com

[o:{0}:p:{1}:r:{2}]
file_filter  = {4}/{2}.<lang>.strings
source_file  = {5}{6}
source_lang  = {3}
type         = STRINGS
minimum_perc = 0
""".format(Organisationskennung, Projektkennung, Ressourcenkennung, Projektsprache, Stringspfad, Strings_Quellpfad, Strings_Quelldatei)
    with open (tx_Verzeichnis + tx_Konfiguration, "w") as tx_conf:
        tx_conf.write(tx_config)

def Transifex_Stringsdateien_senden():
# Unübersetzte Stringsdatei(en) gemäß Sprachenliste zum Transifexserver senden (upload unlocalised strings files to transifex)
    Rückgabewert = subprocess.call(['tx', '-c', tx_Verzeichnis + tx_Konfiguration , 'push', '--silent', '-w', '20', '-f', '-t', '-l', ','.join(Sprachliste), '-r', "{0}.{1}".format(Projektkennung, Ressourcenkennung)])
    if Rückgabewert != 0:
        print(_("Fehler: Das Übertragen der lokalen .strings-Dateien zu transifex ist fehlgeschlagen."))
        exit(1)
    print()

def Transifex_Stringsdateien_holen():
# Übersetzte Stringsdatei(en) gemäß Sprachenliste vom Transifexserver herunterladen (download localised strings files from transifex)
    Rückgabewert = subprocess.call(['tx', '-c', tx_Verzeichnis + tx_Konfiguration , 'pull', '--silent', '-w', '20', '-f', '-t', '-m', 'onlytranslated', '-l', ','.join(Sprachliste), '-r', "{0}.{1}".format(Projektkennung, Ressourcenkennung)])
    if Rückgabewert != 0:
        print(_("Fehler: Das Herunterladen der übersetzten .strings-Dateien von transifex ist fehlgeschlagen."))
        exit(1)
    print()

def Transifex_Stringsquelle_senden():
# Stringsquelle zum Transifexserver senden (upload strings source file to transifex)
    Rückgabewert = subprocess.call(['tx', '-c', tx_Verzeichnis + tx_Konfiguration , 'push', '--silent', '-f', '-s'])
    if Rückgabewert != 0:
        print(_("Fehler: Das Übertragen der lokalen .strings-Quelldateie zu transifex ist fehlgeschlagen."))
        exit(1)
    print()

def Transifex_Stringsquelle_holen():
# Stringsquelldatei vom Transifex server herunterladen (download source strings file from transifex)
    Rückgabewert = subprocess.call(['tx', '-c', tx_Verzeichnis + tx_Konfiguration , 'pull', '--silent', '-s', "{0}.{1}".format(Projektkennung, Ressourcenkennung)])
    if Rückgabewert != 0:
        print(_("Fehler: Das Herunterladen der .strings-Quelldatei von transifex ist fehlgeschlagen."))
        if Optionen.get('Übersetzungsmodus') or Optionen.get('Sicherungsmodus'): exit(1)  # Wenn nicht im Übersetzungsmodus, hier nicht beenden, die Datei wird für die anderen Modi nicht zwingend benötigt. (don't break here, we don't need it urgently when not being running translations mode.)

def Lokale_Stringsdateien_holen(Lokalverzeichnis):
# Bereitgestellte Stringsdateien ins Stringsverzeichnis kopieren (copy locally supplied strings files to strings folder)    
    if not os.path.isdir(Stringspfad):
        # Position des Eintrags: Befehlszeile, Information wenn das Stringsverzeichnis neu erstellt werden muß. Substitution: {0} = Verzeichnisname (String-Position: Command line info message if stringsfolder was to be created. Substitution: {0} = foldername)
        print(_("Erzeuge Stringsverzeichnis {0}").format(Stringspfad))
        os.makedirs(Stringspfad, exist_ok=True)
    if not os.path.isdir(Lokalverzeichnis + '/strings') or len([x for x in (next(os.walk(Lokalverzeichnis + '/strings'), (None, None, []))[2]) if x.endswith('.strings')]) == 0:
        # Position des Eintrags: Befehlszeile, Information wenn das Lokale Quellverzeichnis für Stringdateien neu erstellt werden muß. Substitution: {0} = Verzeichnisname (String-Position: Command line info message if local source stringsfolder was to be created. Substitution: {0} = foldername)
        print(_("Fehler: Lokale Dateien nicht gefunden. Erzeuge lokales Quellverzeichnis für lokalisierte Strings-dateien {0}").format(Lokalverzeichnis + '/strings'))
        os.makedirs(Lokalverzeichnis + '/strings', exist_ok=True)
        exit(1)
    if not os.path.isfile(Lokalverzeichnis + Strings_Quelldatei):
        # Position des Eintrags: Befehlszeile, Fehlermeldung wenn die .strings-Quelldatei im lokalen Ordner nicht gefunden wurde. Substitution: {0} = Dateiname (String-Position: Command line error message if the strings source file in local folder was not found. Substitution: {0} = filename)
        print(_("Fehler: Die lokale Strings-Quelldatei {0} wurde nicht gefunden.").format(Strings_Quelldatei))
        exit(1)
    i=0
    Fehler = False
    Fehlerliste = []
    for Sprache in Sprachliste:
        Stringsdatei = Ressourcenkennung + '.' + Sprache + '.strings'
        try:
            shutil.copy(Lokalverzeichnis + '/strings/' + Stringsdatei, Stringspfad, follow_symlinks=True)
            i += 1
        except:
            Fehlerliste.append(Stringsdatei)
    if len(Fehlerliste) != 0:
        print(Farbe['Rot_Fettdruck'] + _("Fehler: Die Datei(en) {0}konnte(n) nicht im Verzeichnis {1}gefunden werden.").format(Farbe['Rot_Normaldruck'] + "\n    " + "\n    ".join(Fehlerliste) + '\n' + Farbe['Rot_Fettdruck'], '\n    ' + Farbe['Rot_Normaldruck'] + Lokalverzeichnis+ '\n' + Farbe['Rot_Fettdruck']) + Farbe['Weiß_Normaldruck'])
        exit(1)

def Lokale_Stringsdatei_holen(Lokalverzeichnis):
# Bereitgestellte Stringsdateien ins Stringsverzeichnis kopieren (copy locally supplied strings files to strings folder)    
    if not os.path.isdir(Stringspfad):
        # Position des Eintrags: Befehlszeile, Information wenn das Stringsverzeichnis neu erstellt werden muß. Substitution: {0} = Verzeichnisname (String-Position: Command line info message if stringsfolder was to be created. Substitution: {0} = foldername)
        print(_("Erzeuge Stringsverzeichnis {0}").format(Stringspfad))
        os.makedirs(Stringspfad, exist_ok=True)
    if not os.path.isdir(Lokalverzeichnis + '/strings') or len([x for x in (next(os.walk(Lokalverzeichnis + '/strings'), (None, None, []))[2]) if x.endswith('.strings')]) == 0:
        # Position des Eintrags: Befehlszeile, Information wenn das Lokale Quellverzeichnis für Stringdateien neu erstellt werden muß. Substitution: {0} = Verzeichnisname (String-Position: Command line info message if local source stringsfolder was to be created. Substitution: {0} = foldername)
        print(_("Fehler: Lokale Dateien nicht gefunden. Erzeuge lokales Quellverzeichnis für lokalisierte Strings-dateien {0}").format(Lokalverzeichnis + '/strings'))
        os.makedirs(Lokalverzeichnis + '/strings', exist_ok=True)
        exit(1)
    try:
        #for Stringsdatei in [x for x in (next(os.walk(Lokalverzeichnis), (None, None, []))[2]) if x.endswith('.strings')]: shutil.copy(Lokalverzeichnis + '/' + Stringsdatei, Stringspfad, follow_symlinks=True)
        shutil.copy("{0}/strings/{1}.{2}.strings".format(Lokalverzeichnis, Ressourcenkennung, Aktuelle_Sprache), Stringspfad, follow_symlinks=True)
    except:
        # Position des Eintrags: Befehlszeile, Fehlermeldung wenn lokal keine zur Ressource gehörenden Strings-Dateien kopiert werden konnten. Substitution: {0} = Quellverzeichnis {1} = Zielverzeichnis {2} = Tx-Ressourcenkennung (String-position: Error message on console output if no strings files belonging to the resource were found. Substitution {0} = source folder {1} = target folder {2} = tx resource-slug)
        print(_("Eine lokalisierte .strings-Datei der Ressource „{2}” ist nicht im lokalen Verzeichnis „{0}” vorhanden. Es wird eine neue Übersetzung in „{1}” angelegt.").format(Lokalverzeichnis + '/strings', Stringspfad, Ressourcenkennung))

def Lokale_Quelldatei_holen(Lokalverzeichnis):
    if not os.path.isfile(Lokalverzeichnis + Strings_Quelldatei):
        # Position des Eintrags: Befehlszeile, Fehlermeldung wenn die .strings-Quelldatei im lokalen Ordner nicht gefunden wurde. Substitution: {0} = Dateiname (String-Position: Command line error message if the strings source file in local folder was not found. Substitution: {0} = filename)
        print(_("Fehler: Die lokale Strings-Quelldatei {0} wurde nicht gefunden.").format(Strings_Quelldatei))
        exit(1)
    try:
        shutil.copy(Lokalverzeichnis + Strings_Quelldatei, Strings_Quellpfad, follow_symlinks=True)
    except:
        # Position des Eintrags: Befehlszeile, Fehlermeldung wenn lokal die zur Ressource gehörenden Strings-Quelldatei nicht kopiert werden konnte. Substitution: {0} = Quellverzeichnis {1} = Zielverzeichnis {2} = Tx-Ressourcenkennung (String-position: Error message on console output if no strings files belonging to the resource were found. Substitution {0} = source folder {1} = target folder {2} = tx resource-slug)
        print(_("Fehler: Das Kopieren der lokalen .strings-Quelldatei der Ressource „{2}” aus Verzeichnis „{0}” nach „{1}” ist fehlgeschlagen.").format(Lokalverzeichnis, Strings_Quellpfad, Ressourcenkennung))
        exit(1)

#def Debuild_vorbereiten():
#    #(Prepare Debuild)
#    os.makedirs(Paket_Zielpfad + "/DEBIAN/source", exist_ok=True)
#    # Formatdatei aus git übernehmen (copy format file from git) debuild only
#    try:
#        shutil.copy("{0}/{1}/DEBIAN/source/{2}".format(Gitpfad,Ressourcenkennung,"format"), "{0}/DEBIAN/source".format(Paket_Zielpfad), follow_symlinks=True)
#    except:
#        pass
#    # Installationsdatei aus git übernehmen (copy install file from git) debuild only, not dpkg-deb
#    try:
#        shutil.copy("{0}/{1}/DEBIAN/{2}".format(Gitpfad,Ressourcenkennung,"install"), "{0}/{1}".format(Paket_Zielpfad,"DEBIAN"), follow_symlinks=True)
#    except:
#        pass
#    # Kompatibilitätsdatei aus git übernehmen (copy compat file from git) debuild only, not dpkg-deb
#    try:
#        shutil.copy("{0}/{1}/DEBIAN/{2}".format(Gitpfad,Ressourcenkennung,"compat"), "{0}/{1}".format(Paket_Zielpfad,"DEBIAN"), follow_symlinks=True)
#    except:
#        pass
#    # Regeldatei aus git übernehmen (copy rules file from git) debuild only, not dpkg-deb
#    try:
#        shutil.copy("{0}/{1}/DEBIAN/{2}".format(Gitpfad,Ressourcenkennung,"rules"), "{0}/{1}".format(Paket_Zielpfad,"DEBIAN"), follow_symlinks=True)
#    except:
#        pass
#    # Lizenzdatei aus git übernehmen (copy copyright file from git) debuild only, not dpkg-deb
#    try:
#        shutil.copy("{0}/{1}/doc/{2}".format(Gitpfad,Ressourcenkennung,"copyright"), "{0}/{1}".format(Paket_Zielpfad,"DEBIAN"), follow_symlinks=True)
#    except:
#        pass
## Aktualiserungsprotokoll ergänzen (update changelog) by abc-nix (auto-increments version number! not useful for multiple repeated testing packages)
#    with open(Paket_Zielpfad + "/DEBIAN/changelog", "r", opener=opener) as file:
#        first_line = file.readline().strip()
#        existing_content = file.read()
#    # Extract version number
#    version_number = first_line.split()[1].strip('()')
#    # Increase version number and set package date
#    parts = [int(part) for part in version_number.split('.')]
#    parts[-1] += 1
#    version_number = '.'.join(str(part) for part in parts)
#    package_date = subprocess.run("date -R")
#    # New entry line
#    content = "antix23-desktop-files ({0}) bookworm; urgency=low".format(version_number) + "\n\n"
#    content += "  * Update language strings" + "\n\n"
#    content += " -- Robin  {0}".format(package_date)
#    # Add new content to changelog
#    with open(Paket_Zielpfad + "/debian/changelog", "w", opener=opener) as changelog_Datei:
#        changelog_Datei.write(content + '\n\n' + existing_content)

#def Paket_bauen_debuild():
#	# Paket mit debuild, debhelper, devtools, build-essentials etc. verpacken (create package on a developer system) by abc-nix
#    subprocess.run(['debuild', '-us', '-uc', '-b'], cwd=Paket_Zielpfad, check=True)

def Paket_bauen():
	# Paket verpacken (create package) auf normalem antiX-system (runs on default antiX system)
    Rückgabewert = subprocess.run(["dpkg-deb", "--root-owner-group", "--build", Paket_Zielpfad, Paketpfad], check=True)
    # todo: check for errors...
    Rückgabewert = subprocess.run('shasum -b -a512 "{0}/{1}_{2}_all.deb" | sed -n "s/^\\(..*\\) \\*..*\\/\\(..*\\)$/\\1 *\\2/p" > "{0}/{1}_{2}_all.deb.sha512.sum"'.format(Paketpfad, Ressourcenkennung, Paketversion), check=True, shell=True)
    # todo: check for errors...

def Paket_vorbereiten():
# Paketverzeichnis bevölkern (prepare packaging)
    # Verzeichnisgröße ermitteln:
    #Paketgröße = sum(p.stat().st_size for p in Path(Desktop_Zielpfad).rglob('*'))
    Paketgröße  = subprocess.check_output(['du','-sb', Desktop_Zielpfad]).split()[0].decode('utf-8')
    # DEBIAN und doc Unterverzeichnis anlegen (add DEBIAN and doc subfolder if not present)
    os.makedirs(Paket_Zielpfad + "/DEBIAN", exist_ok=True)
    os.makedirs("{0}/usr/share/doc/{1}".format(Paket_Zielpfad,Ressourcenkennung), exist_ok=True)
    try:
        shutil.copy("{0}/{1}/doc/{2}".format(Gitpfad,Ressourcenkennung,"copyright"), "{0}/usr/share/doc/{1}".format(Paket_Zielpfad,Ressourcenkennung), follow_symlinks=True)
    except:
        pass
    # Aktualisierungsprotokoll aus git übernehmen (extract changelog from git)
    try:
        with open(Paket_Zielpfad + "/DEBIAN/changelog", 'wb') as changelog_Datei, gzip.open("{0}/{1}/doc/{2}".format(Gitpfad,Ressourcenkennung,'changelog.gz'), 'rb') as changelog_gzip:
            changelog_Datei.writelines(changelog_gzip)
    except:
        # Position des Eintrags: Befehlszeile, Fehlermeldung wenn lokal keine zur Ressource gehörenden Strings-Dateien kopiert werden konnten. Substitution: {0} = Quellverzeichnis {1} = Zielverzeichnis {2} = Tx-Ressourcenkennung (String-position: Error message on console output if no strings files belonging to the resource were found. Substitution {0} = source folder {1} = target folder {2} = tx resource-slug)
        print(Farbe['Rot_Fettdruck'] + _("Fehler: Keine Aktualisierungsprotkolldatei im Git-Verzeichnis gefunden.")+ Farbe['Weiß_Normaldruck'])
        print(_("Erstelle leere Protokolldatei {0}/DEBIAN/{1}").format(Paket_Zielpfad, "changelog"))
        #os.mknod("{0}/DEBIAN/{1}".format(Paket_Zielpfad, "changelog"))
    # Aktualisierungsprotokoll ergänzen (update changelog)
    with open(Paket_Zielpfad + "/DEBIAN/changelog", "r", opener=opener) as changelog_Datei:
        existing_content = changelog_Datei.read()
    content = "{0} ({1}) bookworm; urgency=low".format(Paket,Paketversion) + "\n\n"
    if Optionen.get('Aktualisierungsmodus'):
        content += "  * Update translated strings from Transifex, {0}".format(Datum) + "\n\n"
    elif Optionen.get('Quellaktualisierungsmodus'):
        content += "  * Update source strings, {0}".format(Datum) + "\n\n"
    content += " -- {0}  {1}".format(Maintainer,Datum)
    with open(Paket_Zielpfad + "/DEBIAN/changelog", "w", opener=opener) as changelog_Datei:
        changelog_Datei.write(content + '\n\n' + existing_content)
    # Aktualisierungsprotokoll verpacken (compress changelog)
    try:
        os.remove("{0}/usr/share/doc/{1}/changelog.gz".format(Paket_Zielpfad,Ressourcenkennung))
    except:
        pass
    with open(Paket_Zielpfad + "/DEBIAN/changelog", 'rb') as changelog_Datei, gzip.open("{0}/usr/share/doc/{1}/changelog.gz".format(Paket_Zielpfad,Ressourcenkennung), 'wb') as changelog_gzip:
        changelog_gzip.writelines(changelog_Datei)
    try:
        # Nur für dpkg-deb, nicht für debuild benötigt (only needed for dpkg-deb, not for debuild):
        os.remove(Paket_Zielpfad + "/DEBIAN/changelog")
    except:
        pass
    # Datei „preinst” erstellen (write preinst file)
      # (nicht erforderlich, not needed)
    # Datei „postinst” erstellen (write postinst file)
    shutil.copy("{0}/{1}/DEBIAN/postinst".format(Gitpfad, Ressourcenkennung), "{0}/DEBIAN".format(Paket_Zielpfad))
    # Datei „prerm” erstellen (write prerm file)
    shutil.copy("{0}/{1}/DEBIAN/prerm".format(Gitpfad, Ressourcenkennung), "{0}/DEBIAN".format(Paket_Zielpfad))
    # Datei „postrm” erstellen (write postrm file)
      # (nicht erforderlich, not needed)
    # Datei „control” erstellen (write control file)
    content = "Package: {0}".format(Paket) + "\n"
    content += "Version: {0}".format(Paketversion) + "\n"
    content += "Section: localization"  + "\n"
    content += "Priority: optional"  + "\n"
    content += "Architecture: all" + "\n"
    content += "Depends: sed, grep, bash, dpkg" + "\n"
    content += "Installed-Size: {0}".format(str(int(int(Paketgröße)/1024))) + "\n"
    content += "Replaces: {0} (<< {1})".format(Paket, Paketversion) + "\n"
    content += "Maintainer: {0}".format(Maintainer) + "\n"
    content += "Homepage: {0}".format(Homepage) + "\n"
    content += "Description: antiX .desktop files localisation for antiX 23" + "\n"
    with open(Paket_Zielpfad + "/DEBIAN/control", "w", opener=opener) as control_Datei:
        control_Datei.write(content)
    # Paketverzeichnisse bevölkern (create and populate package folders)
    os.makedirs(Paket_Zielpfad + "/usr/local/lib/antiX/" + Ressourcenkennung , exist_ok=True)
    shutil.copytree(Desktop_Zielpfad, "{0}/usr/local/lib/antiX/{1}/reservoir".format(Paket_Zielpfad,Ressourcenkennung))
    # Fix: nach antiX 23-testing gibt es kein /antix Unterverzeichnis mehr in applications (there is no longer an antix subfolder present in applications):
    if not os.listdir(Paket_Zielpfad + "/usr/local/lib/antiX/" + Ressourcenkennung + "/reservoir"):
        os.rmdir("{0}/usr/local/lib/antiX/{1}/reservoir/antix".format(Paket_Zielpfad,Ressourcenkennung))
    # Prüfsummendatei erzeugen (calculate checksums file)
    Desktopdateien = [os.path.join(Verzeichnis, Datei).replace(Paket_Zielpfad + "/", "")
        for Verzeichnis, V, Dateien in os.walk(Paket_Zielpfad)
        for Datei in Dateien
        if not "/DEBIAN" in Verzeichnis]
    content = ""
    for Eintrag in Desktopdateien:
        with open("{0}/{1}".format(Paket_Zielpfad,Eintrag), "rb") as Prüfdatei:
            Dateihash = hashlib.md5()
            while Datenblock := Prüfdatei.read(8192):
                Dateihash.update(Datenblock)
        content += "{0} {1}".format(Dateihash.hexdigest(),Eintrag) + "\n"
    with open(Paket_Zielpfad + "/DEBIAN/md5sums", "w", opener=opener) as md5sums_Datei:
        md5sums_Datei.write(content)
    # Paket zusammenpacken (create package)
    Paket_bauen()
    #Paket_bauen_debuild()
    
def Git_Vorbereiten():
# Stringsverzeichnis, Applikationsverzeichnis und Paketdateien ins Gitverzeichnis kopieren (Copy strings folder, applications folder and paket files to cloned git folder)
    try:
        shutil.rmtree("{0}/{1}/applications".format(Gitpfad,Ressourcenkennung))
    except:
        pass
    try:
        shutil.rmtree("{0}/{1}/strings".format(Gitpfad,Ressourcenkennung))
    except:
        pass
    try:
        shutil.rmtree("{0}/{1}/deb-paket".format(Gitpfad,Ressourcenkennung))
    except:
        pass
    try:
        os.remove("{0}/{1}".format(Gitpfad,Ressourcenkennung) + Strings_Quelldatei)
    except:
        pass
    try:
        shutil.rmtree("{0}/{1}".format(Gitpfad,Ressourcenkennung) + "DEBIAN")
    except:
        pass
    try:
        shutil.rmtree("{0}/{1}/{2}".format(Gitpfad,Ressourcenkennung,'doc'))
    except:
        pass
    try:
        shutil.copytree(Txpfad, "{0}/{1}".format(Gitpfad,Ressourcenkennung), dirs_exist_ok=True)
    except:
        # Position des Eintrags: Befehlszeile, Fehlermeldung wenn lokal die zur Ressource gehörenden Strings-Quelldatei nicht kopiert werden konnte. Substitution: {0} = Quellverzeichnis {1} = Zielverzeichnis {2} = Tx-Ressourcenkennung (String-position: Error message on console output if no strings files belonging to the resource were found. Substitution {0} = source folder {1} = target folder {2} = tx resource-slug)
        print(Farbe['Rot_Fettdruck'] + _("Fehler: Das Kopieren der lokalen .strings-Quelldatei der Ressource „{2}” aus Verzeichnis „{0}” nach „{1}” ist fehlgeschlagen.").format(Txpfad, Gitpfad, Ressourcenkennung) + Farbe['Weiß_Normaldruck'])
        exit(1)
    try:
        os.makedirs("{0}/{1}/deb-paket".format(Gitpfad, Ressourcenkennung), exist_ok=True)
        shutil.copy("{0}/{1}".format(Paketpfad, Paketdatei), "{0}/{1}/deb-paket/{2}".format(Gitpfad, Ressourcenkennung, Paketdatei))
        shutil.copy("{0}/{1}{2}".format(Paketpfad, Paketdatei, '.sha512.sum'), "{0}/{1}/deb-paket/{2}.sha512.sum".format(Gitpfad, Ressourcenkennung, Paketdatei))
    except:
        print(Farbe['Rot_Fettdruck'] + _("Fehler: Die Paketdatei {0}konnte nicht im Verzeichnis {1}gefunden werden.").format(Farbe['Rot_Normaldruck'] + "\n    " + "\n    " + Paketdatei + '\n' + Farbe['Rot_Fettdruck'], '\n    ' + Farbe['Rot_Normaldruck'] + Paketpfad + '\n' + Farbe['Rot_Fettdruck']) + Farbe['Weiß_Normaldruck'])
    try:    
        shutil.copytree("{0}/{1}".format(Paket_Zielpfad,'DEBIAN'), "{0}/{1}/DEBIAN".format(Gitpfad, Ressourcenkennung), dirs_exist_ok=True)
    except:
        print(Farbe['Rot_Fettdruck'] + _("Fehler: Das DEBIAN-Verzeichnis konnte nicht im Verzeichnis {1}gefunden werden.").format('\n    ' + Farbe['Rot_Normaldruck'] + Paketpfad + '\n' + Farbe['Rot_Fettdruck']) + Farbe['Weiß_Normaldruck'])
    shutil.copytree("{0}/usr/share/doc/{1}".format(Paket_Zielpfad,Ressourcenkennung), "{0}/{1}/doc".format(Gitpfad, Ressourcenkennung), dirs_exist_ok=True)

def Git_Senden():
# Übertagen aller vorbereiteten Dateien an Gitlab (Transfer prepared files to gitlab)
    subprocess.call(['git', '-C', "{0}/{1}".format(Gitpfad, Ressourcenkennung), 'add', '-A'])
    print("Update {0} from Transifex ({1})".format(Ressourcenkennung , Datum))
    subprocess.call(['git', '-C', "{0}/{1}".format(Gitpfad, Ressourcenkennung), 'commit', '-m', "Update {0} from Transifex ({1})".format(Ressourcenkennung , Datum)])
    Rückgabewert = subprocess.call(['git', '-C', "{0}/{1}".format(Gitpfad, Ressourcenkennung), 'push'])
    if Rückgabewert != 0:
        print(_("Die Ressource {0} kann nicht übertragen werden.").format(Homepage))
        exit(1)

def TX_Sicherungskopie():
# Lokale Sicherungskopie der auf Transifex vorhandenen Dateien anfertigen, bevor Änderungen der Ressource auf Transifex vorgenommen werden. (make backup of remote resource before starting with modifications)
    # Schreibe ressourcenspezifische .tx/config Datei (write .tx/config resource specifically)
    os.makedirs(tx_Verzeichnis, exist_ok=True)
    tx_config = """[main]
host = https://www.transifex.com

[o:{0}:p:{1}:r:{2}]
file_filter  = {4}/{2}.<lang>.strings
source_file  = {5}{6}
source_lang  = {3}
type         = STRINGS
minimum_perc = 0
""".format(Organisationskennung, Projektkennung, Ressourcenkennung, Projektsprache, Sicherungspfad + '/strings', Sicherungspfad, Strings_Quelldatei)
    with open (tx_Verzeichnis + tx_Konfiguration + '-backup', "w") as tx_conf:
        tx_conf.write(tx_config)
    # Stringsquelldatei vom Transifex server herunterladen (download source strings file from transifex)
    Rückgabewert = subprocess.call(['tx', '-c', tx_Verzeichnis + tx_Konfiguration + '-backup' , 'pull', '--silent', '-s', "{0}.{1}".format(Projektkennung, Ressourcenkennung)])
    #Rückgabewert = subprocess.call('bash -c \'tx -c ' + tx_Verzeichnis + tx_Konfiguration + '-backup pull -s ' + "{0}.{1}".format(Projektkennung, Ressourcenkennung) + ' | cat; exit ${PIPESTATUS[0]}\'', shell=True)  # Workaround to stop console spam from tx client
    if Rückgabewert != 0 or not os.path.isfile(Sicherungspfad + Strings_Quelldatei):
        print(Farbe['Rot_Fettdruck'] + _("Fehler: Das Herunterladen der .strings-Quelldatei von transifex ist fehlgeschlagen.") + Farbe['Weiß_Normaldruck'])
        exit(1)
    # Alle Stringsdatei(en) vom Transifexserver herunterladen (download all localised strings files from transifex)
    Rückgabewert = subprocess.call(['tx', '-c', tx_Verzeichnis + tx_Konfiguration + '-backup', 'pull', '--silent', '-w', '20', '-f', '-t', '-a', '-m', 'onlytranslated', "{0}.{1}".format(Projektkennung, Ressourcenkennung)])
    #Rückgabewert = subprocess.call('bash -c \'tx -c ' + tx_Verzeichnis + tx_Konfiguration + '-backup pull -f -t -a -m onlytranslated ' + "{0}.{1}".format(Projektkennung, Ressourcenkennung) + ' | cat; exit ${PIPESTATUS[0]}\'', shell=True)
    if Rückgabewert != 0:
        print(Farbe['Rot_Fettdruck'] + _("Fehler: Das Herunterladen der übersetzten .strings-Dateien von transifex ist fehlgeschlagen.") + Farbe['Weiß_Normaldruck'])
        exit(1)
    print()

def Desktop_Sicherungskopie():
    # Aktuell vorliegende .desktop-Dateien ins Sicherungsverzeichnis kopieren (Copy recent desktop files to backup folder)
    os.makedirs(Desktop_Sicherungspfad, exist_ok=True)
    shutil.copytree(Desktop_Quellpfad, Desktop_Sicherungspfad + "/applications")

def Git_Sicherungskopie():
    # Aktuelle Daten von Gitlab sichern
    print('\n' + Farbe['Blau_Fettdruck'] + '-------------------------\n* ' + _("Klone Daten von Gitlab") + '\n-------------------------\n' + Farbe['Weiß_Normaldruck'])
    Rückgabewert = subprocess.call(['wget', '-q', '--method=HEAD', Gitpage])
    if Rückgabewert == 0:
        os.makedirs(Git_Sicherungspfad, exist_ok=True)
        Rückgabewert = subprocess.call(['git', '-C', Git_Sicherungspfad, 'clone', Gitpage])
        if Rückgabewert != 0:
            print(_("Die Ressource {0} kann nicht geklont werden.").format(Gitpage))
            exit(1)
    else:
        print(_("Die Ressource {0} existiert nicht.").format(Gitpage))
        exit(1)

def Antivalente_Disjunktion(aut_Gruppe, Elemente):
# Exklusiv-Oder-Gatter für sich ausschließende Befehlszeilenoptionen (XOR gate for contradicting input options)
    if True in aut_Gruppe:
        aut_Gruppe=iter(aut_Gruppe)
        if not(any(aut_Gruppe) and not any(aut_Gruppe)):
            # Position des Eintrags: Befehlszeile, Fehlermeldung wenn inkompatible Optionen angegeben wurden. Substitution: {0} = Liste der inkompatiblen Optionen. (String-position: Error message on console output if two or more incompatible options were filed. Substitution {0} = list of the respective options)
            print(_("Von den Optionen {0} darf nur jeweils eine angegeben werden.").format(Elemente))
            exit(1)

def opener(path, flags):
# Korrekte Dateirechte beim Schreiben verwenden (set proper permissions when writing files)
    return os.open(path, flags, 0o755)

def Farben():
# Konsolenschriftfarben und Auszeichnungen definieren (set console font colour)
    global Farbe
    Farbe = {
        'Weiß_Fettdruck'   : '\033[1;37m',
        'Weiß_Normaldruck' : '\033[0;37m',
        'Grün_Fettdruck'   : '\033[1;32m',
        'Grün_Normaldruck' : '\033[0;32m',
        'Blau_Fettdruck'   : '\033[1;34m',
        'Blau_Normaldruck' : '\033[0;34m',
        'Rot_Fettdruck'    : '\033[1;31m',
        'Rot_Normaldruck'  : '\033[0;31m',
        'Gelb_Fettdruck'    : '\033[1;33m',
        'Gelb_Normaldruck'  : '\033[0;33m',
    }

def Optionen_evaluieren():
# Befehlszeilenoptionen auslesen und Hilfetexte bereitstellen (parse command line input and define help messages)
    global Optionen
    Gliederungsstruktur = argparse.ArgumentParser(prog=sys.argv[0].split(os.sep)[-1],
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Eingabemuster, einzelnes Wort in eckigen Klammern. (String-Position: Command line help message, usage line, single word in square brackets.)
        usage='%(prog)s [' + _("options") + ']',
        add_help=False,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Kopfeintrag. (String-Position: Command line help message, header entry.)
        description=_("Das Script aktualisiert .desktop-Dateien mit Übersetzungen aus .strings-Dateien von Transifex."))
        #formatter_class=split_formatter)
    # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Abschnittsüberschrift. (String-Position: Command line help message, section header entry.)
    Hauptgruppe = Gliederungsstruktur.add_argument_group(_("Hauptoptionen"))
    # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Abschnittsüberschrift. (String-Position: Command line help message, section header entry.)
    Konfigurationsgruppe = Gliederungsstruktur.add_argument_group(_("Konfigurationsoptionen"))
    # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Abschnittsüberschrift. (String-Position: Command line help message, section header entry.)
    Zusatzgruppe = Gliederungsstruktur.add_argument_group(_("Weitere Optionen"))
    # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Abschnittsüberschrift. (String-Position: Command line help message, section header entry.)
    Informationsgruppe = Gliederungsstruktur.add_argument_group(_("Hilfe und Information"))
# Hauptgruppe (Main group):
    Hauptgruppe.add_argument('-a', '--' + _('Aktualisieren'),
        dest='Aktualisierungsmodus',
        action='store_true',
        default=False,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Aktualisieren” (String-Position: Command line help message, description of option -a)
        help=_("Aktualisieren lokaler .desktop-Dateien mit Übersetzungen aus .strings-Dateien von Transifex. \
Ohne zusätzliche Angabe der Option -l werden die Dateien vom Server geholt."))
    Hauptgruppe.add_argument('-e', '--' + _('Extrahieren'),
        dest='Extraktionsmodus',
        action='store_true',
        default=False,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Extrahieren” (String-Position: Command line help message, description of option -e)
        help=_("Extrahieren der übersetzbaren Zeichenketten aus den im {0} Unterverzeichnis des Ressourcenverzeichnisses \
bereitgestellten .desktop dateien entsprechend der Sprachenliste und Übertragen in .strings-Dateien. Mit zusätzlicher Angabe der Option -HL werden die \
.strings Quelldatei und die zugehörigen Sprachdateien auf dem Transifex Server aktualisiert, wobei alle vorhandenen Übersetzungen mit den neu \
extrahierten Einträgen überschrieben werden.").format('applications'))
    Hauptgruppe.add_argument('-q', '--' + _('Quellaktualisierung'),
        dest='Quellaktualisierungsmodus',
        action='store_true',
        default=False,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Quellaktualisierung” (String-Position: Command line help message, description of option -q)
        help=_("Vollständiges Aktualisieren der Ressource auf Transifex mit Einträgen aus geänderten oder neuen lokalen .desktop-Dateien. \
Mit zusätzlicher Angabe der Option -HL werden die aktualisierte .strings Quelldatei und die zugehörigen Sprachdateien an Transifex übertragen, wobei keine auf \
Transifex bereits vorhandenen Übersetzungen überschrieben werden."))
    Hauptgruppe.add_argument('-ü', '--' + _('Übersetzen'),
        dest='Übersetzungsmodus',
        action='store_true',
        default=False,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Übersetzen” (String-Position: Command line help message, description of option -ü)
        help=_("Übersetzen der Einträge auf Transifex."))
    Hauptgruppe.add_argument('-s', '--' + _('Sicherung'),
        dest='Sicherungsmodus',
        action='store_true',
        default=False,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Übersetzen” (String-Position: Command line help message, description of option -ü)
        help=_("Sichern der vollständigen Ressource von Transifex (Strings-Quelldatei und alle Strings-Übersetzungsdateien).") + " " + _("Bei zusätzlicher \
Angabe von {0} werden auch alle Daten der Ressource von Gitlab lokal gesichert.".format('-g')))
#Konfigurationsgruppe (config group):
    Konfigurationsgruppe.add_argument('-to', '--' + _('Tx-Organisation='),
        dest='Organisationskennung',
        metavar="<" + _("Organisationskennung") + ">",
        action='store',
        nargs=1,
        default=['antix-linux-community-contributions'],
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Tx-Organisation=” Substitution: {0} = Transifex-Organisationskennung (String-Position: Command line help message, description of option -to (Substitutions: {0} = transifex organisation-slug)
        help=_("Transifex Organisationskennung. Ohne Angabe wird „{0}” verwendet.").format('antix-linux-community-contributions'))
    Konfigurationsgruppe.add_argument('-tp','--' + _('Tx-Projekt='),
        dest='Projektkennung',
        metavar="<" + _("Projektkennung") + ">",
        action='store',
        nargs=1,
        default=['antix-contribs'],
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Tx-Projekt=” Substitution: {0} = Transifex-Projektkennung (String-Position: Command line help message, description of option -tp (Substitutions: {0} = transifex project-slug)
        help=_("Transifex Projektkennung. Ohne Angabe wird „{0}” verwendet.").format('antix-contribs'))
    Konfigurationsgruppe.add_argument('-tr','--' + _('Tx-Ressource='),
        dest='Ressourcenkennung',
        metavar="<" + _("Ressourcenkennung") + ">",
        action='store',
        nargs=1,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Tx-Ressource=” Substitution: {0} = Transifex-Ressourcenkennung (String-Position: Command line help message, description of option -tr (Substitutions: {0} = transifex resource-slug)
        help=_("Transifex Ressourcenkennung (z.B. „{0}”)").format('antix-desktop-files'))
    Konfigurationsgruppe.add_argument('-ts', '--' + _('Tx-Sprachkennung='),
        dest='Projektsprache',
        metavar="<" + _("Transifex-Sprachkennung") + ">",
        action='store',
        nargs=1,
        default=['en@pirate'],
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Tx-Sprachkennung=” Substitution: {0} = Transifex-Projektsprachkennung (String-Position: Command line help message, description of option -ts (Substitutions: {0} = transifex project-language-identifier)
        help=_("Transifex Projektsprachkennung. Ohne Angabe wird „{0}” verwendet. Dabei steht „{0}” nicht \
für Englisch, sondern es ist ein Stellvertreter bzw. Platzhalter für die jeweilige wahre Sprache (Originalsprache) \
der Ressource.)").format('en@pirate'))
    Konfigurationsgruppe.add_argument('-ws','--' + _('Wahre-Sprache='),
        dest='Ressourcensprache',
        metavar="<" + _("Sprachidentifikator") + ">",
        action='store',
        nargs=1,
        default=['en'],
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Wahre-Sprache=” Substitution: {0} = Identifikator der wahren Sprache bzw. Originalsprache der Ressource, {1} = Transifex-Projektsprachkennung {2} = Option -ts (String-Position: Command line help message, description of option -ws (Substitutions: {0} = true language ID, {1} = transifex project-language-identifier {2} = option -ts)
        help=_("Kennung der wahren Sprache einer Projektressource. Ohne Angabe wird „{0}” verwendet. Es sind \
Identifikatoren gem. ISO 3166 und ISO 639 im UNIX Format für die tatsächliche Sprache (Originalsprache) anzugeben, \
für die die Projektsprachkennung ({1}) bei der mit {2} angegebenen Ressource des Projekts steht. Beispiele: fr_BE, \
sv, pt_BR, haw, zh_TW)").format('en','en@pirate', '--' + _('Tx-Ressource=')))
# Zusatzgruppe (additional options group):
    Zusatzgruppe.add_argument('-lq', '--' + _('Lokale_Quellen'),
        dest='Lokal',
        action='store_true',
        default=False,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Zusatzoption „--Lokale_Quellen=” (String-Position: Command line help message, description of additional option -lq)
        help=_("Stringsdateien nicht von Transifex holen. Bei den Optionen „{0}” und „{1}” \
werden stattdessen lokale Kopien aus dem im Ressourcenverzeichnis bereitzustellenden Unterverzeichnis „{2}” \
verwendet.").format('--' + _('Aktualisieren'),'--' + _('Extrahieren'), 'locale'))
    Zusatzgruppe.add_argument('-HL', '--' + _('Hochladen'),
        dest='Nicht_Senden',
        action='store_false',
        default=True,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Zusatzoption „--Hochladen=” (String-Position: Command line help message, description of additional option -HL)
        help=_("Verarbeitete Stringsdateien zu Transifex übertragen. Gitlab mit den neuen Daten aktualisieren, sofern die Option {0} angegeben wurde.").format('-HL'))
    Zusatzgruppe.add_argument('-g', '--' + _('Gitlab'),
        dest='Gitlab',
        metavar="<" + _("Gitpage") + ">",
        action='store',
        nargs=1,
        default=False,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Zusatzoption „--Gitlab” (String-Position: Command line help message, description of additional option -g)
        help=_("Die .desktop- und .strings-Dateien sowie das Paket werden nach der Verarbeitung bzw. Aktualisierung für die \
Übertragung an den Gitlab Server vorbereitet. Wird zusätzlich die Option {0} angegeben, werden die vorbereiteten Daten an Gitlab übertragen.").format('-HL'))
    Zusatzgruppe.add_argument('-pn', '--' + _('Paket'),
        dest='Paket',
        metavar="<" + _("Paketname") + ">",
        action='store',
        nargs=1,
        default=False,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Zusatzoption „--Paket” (String-Position: Command line help message, description of additional option -p)
        help=_("Anstelle der Ressourcenkennung den mit dieser Option angegebenen Paketnamen für den \
Paketbau verwenden."))
    Zusatzgruppe.add_argument('-pv', '--' + _('Paketversion'),
        dest='Paketversion',
        metavar="<" + _("Versionsnummer") + ">",
        action='store',
        nargs=1,
        default=['1.0'],
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Zusatzoption „--Paket” (String-Position: Command line help message, description of additional option -p)
        help=_("Paketversion für den Paketbau. Ohne Angabe wird »1.0« verwendet."))
    Zusatzgruppe.add_argument('-pm', '--' + _('Maintainer'),
        dest='Maintainer',
        metavar="<" + _("Name ‹Emailadresse›") + ">",
        action='store',
        nargs=1,
        default=['none'],
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Zusatzoption „--Paket” (String-Position: Command line help message, description of additional option -p)
        help=_("Name und Emailadresse im Format RFC822 für den Paketbau. Ohne Angabe wird »none« verwendet."))
    Zusatzgruppe.add_argument('-ph', '--' + _('Homepage'),
        dest='Homepage',
        metavar="<" + _("URL") + ">",
        action='store',
        nargs=1,
        default=['none'],
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Zusatzoption „--Paket” (String-Position: Command line help message, description of additional option -p)
        help=_("Pakethomepage für den Paketbau. Ohne Angabe wird »none« verwendet."))
# Informations- und Hilfegruppe (group "help and information"):
    Informationsgruppe.add_argument('-h', '--' + _('Hilfe'),
        action='help',
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Hilfe” (String-Position: Command line help message, description of -h option)
        help=_("Zeigt diese Gebrauchsinformationen an."))
    Informationsgruppe.add_argument('-ver', '--' + _('Version'),
        action='version',
        version='%(prog)s ' + version,
        # Position des Eintrags: Befehlszeile, Augabe des Hilfetextes, Beschreibung der Option „--Version” (String-Position: Command line help message, description of -v option)
        help=_("Zeigt die Scriptversion an."))
# Evaluation und Nachlese (Evaluation and Aftermath)
    Optionen = vars(Gliederungsstruktur.parse_args())
    # Die von argparse bereitgestellte Methode vollständig disjunkter Gruppen erlaubt derzeit noch keine Abschnittsüberschriften (builtin mutually exclusive grup in argparse doesn't support section header by now.)
    aut_Gruppe_01 = [Optionen.get('Übersetzungsmodus'),Optionen.get('Extraktionsmodus'),Optionen.get('Aktualisierungsmodus'),Optionen.get('Quellaktualisierungsmodus'),Optionen.get('Sicherungsmodus')]
    aut_Gruppe_01_Elemente='-e, -a, -ü, -q, -s'
    Antivalente_Disjunktion(aut_Gruppe_01, aut_Gruppe_01_Elemente)
    if not (Optionen.get('Übersetzungsmodus') or Optionen.get('Extraktionsmodus') or Optionen.get('Aktualisierungsmodus') or  Optionen.get('Quellaktualisierungsmodus'),Optionen.get('Sicherungsmodus')):
        # Position des Eintrags: Befehlszeile, Fehlermeldung wenn keine der Hauptoptionen ({0} = --Extrahieren, {1} = --Übersetzen oder {2} = --Aktualisieren) angegeben wurde. (String-Position: Command line error message if non of the main options -e, -ü or -a was filed.)
        print(_("Der Betriebsmodus muß zwingend angegeben werden, entweder {0}, {1}, {2}, {3} oder {4}.").format('-e', '-ü', '-a', '-q', '-s'))
        exit(1)
    elif not Optionen.get('Ressourcenkennung'):
        # Position des Eintrags: Befehlszeile, Fehlermeldung wenn keine Transifex-Ressourcenkennung angegeben wurde. Substitution: {0} = Option (String-Position: Command line error message if no transifex resource-slug was filed.)
        print(_("Die Angabe einer Transifex-Ressourcenkennung mit Option {0} ist zwingend erforderlich.").format('-tr'))
        exit(1)
    elif Optionen.get('Gitlab') and not (Optionen.get('Aktualisierungsmodus') or Optionen.get('Sicherungsmodus')):
        print(_("Die Option {0} kann nur in den Betriebsmodi {1} oder {2} verwendet werden.").format('-g','-a','-s'))
        exit(1)
    elif Optionen.get('Gitlab') and Optionen.get('Homepage')[0] == 'none':
        print(_("Bei Angabe der Option {0} muß zwingend die Pakethomepage mit der Option {1} zugewiesen werden.").format('-g','-ph'))
        exit(1)
    elif Optionen.get('Lokal') and not Optionen.get('Nicht_Senden'):
        print(_("Die Option {0} kann nicht zusammen mit der Option {1} verwendet werden.").format('-HL','-lq'))
        exit(1)
    elif Optionen.get('Sicherungsmodus') and not Optionen.get('Nicht_Senden'):
        print(_("Im Betriebsmodus {0} kann die Option {1} nicht verwendet werden.").format('-s','-HL'))
        exit(1)


## scrpt starts here! ##

Farben()
Optionen_evaluieren()

Organisationskennung = Optionen.get('Organisationskennung')[0]
Projektkennung = Optionen.get('Projektkennung')[0]
Ressourcenkennung = Optionen.get('Ressourcenkennung')[0]
Projektsprache = Optionen.get('Projektsprache')[0]
Ressourcensprache = Optionen.get('Ressourcensprache')[0]
if not Optionen.get('Paket'):
    Paket = Ressourcenkennung
else:
    Paket = Optionen.get('Paket')[0]
Paketversion = Optionen.get('Paketversion')[0]
Maintainer = Optionen.get('Maintainer')[0]
Homepage = Optionen.get('Homepage')[0]
if not Optionen.get('Gitlab'):
    Gitpage = 'https://gitlab.com/antix-contribs/antix23-desktop-files'
else:
    Gitpage = Optionen.get('Gitlab')[0]
Nicht_Senden = Optionen.get('Nicht_Senden')
Lokal = Optionen.get('Lokal')

if not Nicht_Senden:
    Kennung = '**'
else:
    Kennung = ""

if Optionen.get('Extraktionsmodus'):
    Arbeitsmodus = _('Extraktion') + Kennung
elif Optionen.get('Aktualisierungsmodus'):
    Arbeitsmodus = _('Aktualisierung') + Kennung
elif Optionen.get('Quellaktualisierungsmodus'):
    Arbeitsmodus = _('Quellaktualisierung') + Kennung
elif Optionen.get('Übersetzungsmodus'):
    Arbeitsmodus = _('Übersetzung') + Kennung
elif Optionen.get('Sicherungsmodus'):
    Arbeitsmodus = _('Sicherung')
Datum = datetime.now().strftime("%d.%m.%Y-%H:%M:%S")
Datumsanhang = "{0}-({1})".format(Datum, Arbeitsmodus)
Ressourcenpfad = "./{0}/{1}".format(Projektkennung, Ressourcenkennung)
Arbeitspfad = "{0}/{1}".format(Ressourcenpfad, Datumsanhang)
tx_Verzeichnis = "{0}/{1}/.tx".format(Projektkennung, Ressourcenkennung)
tx_Konfiguration = '/config'
Txpfad = "{0}/transifex/{1}".format(Arbeitspfad, Ressourcenkennung)
Gitpfad = "{0}/git".format(Arbeitspfad)
Git_Sicherungspfad = Arbeitspfad + '/Git-Sicherungskopie'
Stringspfad = Txpfad + '/strings'
Strings_Quellpfad = Txpfad
Strings_Quelldatei = "/{0}.strings".format(Ressourcenkennung)
if Optionen.get('Aktualisierungsmodus'):
    Desktop_Quellpfad = "{0}/{1}/{2}/{3}".format(Gitpfad,Ressourcenkennung,'originale','applications')
else:
    Desktop_Quellpfad = "{0}/{1}".format(Ressourcenpfad, 'applications')
Desktop_Zielpfad = Txpfad + '/applications'
Desktop_Sicherungspfad = Arbeitspfad + '/Desktop-Sicherungskopie'
Paketpfad = Arbeitspfad + '/paket'
Paket_Zielpfad = "{0}/{1}".format(Paketpfad, Ressourcenkennung)
Paketdatei = "{0}_{1}_all.deb".format(Ressourcenkennung,Paketversion)
Sicherungspfad = Arbeitspfad + '/Transifex-Sicherungskopie'
Lokales_Verzeichnis = Ressourcenpfad + '/local'
if Optionen.get('Aktualisierungsmodus'):
    Sprachdatei = "{0}/{1}/{2}".format(Gitpfad,Ressourcenkennung,"sprachen.lst")
else:
    Sprachdatei =  "./sprachen.lst"
Haupt_Wörterbuch = {}
Eintragszähler = 0


# Prüfen ob alle Voraussetzungen für den Prozess gegeben sind (check whether all folders and files needed are present or create them)
if not Optionen.get('Sicherungsmodus'):
    Voraussetzungen_01()

# Ressourcenspezifische Transifexkonfiguration bereitstellen (Supply resource specific transifex configuration)
Transifex_Initiieren()

if Optionen.get('Gitlab') and not Optionen.get('Sicherungsmodus'):
    print('\n' + Farbe['Blau_Fettdruck'] + '-------------------------\n* ' + _("Klone Daten von Gitlab") + '\n-------------------------\n' + Farbe['Weiß_Normaldruck'])
    Rückgabewert = subprocess.call(['wget', '-q', '--method=HEAD', Gitpage])
    if Rückgabewert == 0:
        Rückgabewert = subprocess.call(['git', '-C', Gitpfad, 'clone', Gitpage])
        if Rückgabewert != 0:
            print(_("Die Ressource {0} kann nicht geklont werden.").format(Gitpage))
            exit(1)
    else:
        print(_("Die Ressource {0} existiert nicht.").format(Gitpage))
        exit(1)

Desktopdateiliste = [re.sub('.desktop$', '', x) for x in (next(os.walk(Desktop_Quellpfad), (None, None, []))[2]) if x.endswith('.desktop')]
Desktopdateiliste += [re.sub('.desktop$', '', x) for x in (next(os.walk(Desktop_Quellpfad + '/antix'), (None, None, []))[2]) if x.endswith('.desktop')] # Zusatz für antix Unterverzeichnis (amendment for antix subfolder)

if not Optionen.get('Sicherungsmodus'):
    Voraussetzungen_03()

if Optionen.get('Extraktionsmodus'):
# todo: add an extra security check to prevent unintended overwriting of existing translations at transifex if the resource exists already. The -e Option should be used only to create a new resource or completely replace an existing resource, setting all translations back to the ones found in the original .desktop files.
    if not Optionen.get('Nicht_Senden'):
        print('\n' + Farbe['Blau_Fettdruck'] + '-------------------------\n* ' + _("Erzeuge Sicherungskopie") + '\n-------------------------\n' +  Farbe['Weiß_Normaldruck'])
        TX_Sicherungskopie()
    # Aktuell vorliegende .desktop-Dateien ins TX-verzeichnis kopieren (Copy recent desktop files to Tx app folder)
    shutil.copytree(Desktop_Quellpfad, Desktop_Zielpfad, dirs_exist_ok=True)    
    # Einlesen der Sprachen aus der Sprachdatei in eine globale Liste (read locale file content into global locale list)
    Sprachdatei_lesen()
    # Einlesen aller übersetzbaren Einträge aus den Desktopdateien in das Hauptwörterbuch (read all translatable desktop files strings into main dictionary)
    print('\n' + Farbe['Blau_Fettdruck'] + '----------------------------------\n* ' + _("Extrahiere übersetzbare Einträge") + '\n----------------------------------\n' + Farbe['Weiß_Fettdruck'] + _("Verarbeite {0} .desktop-Dateien").format(len(Desktopdateiliste)) + Farbe['Weiß_Normaldruck'])
    Eintragszähler = 0
    Sprachliste.append(Projektsprache) #(we want to process the source strings file the same way, so Quellstrings_lesen is deprecated now)
    for Aktuelle_Ressource in Desktopdateiliste:
        parse("\n".join(Desktopdatei_lesen("/{0}.desktop".format(Aktuelle_Ressource))), Aktuelle_Ressource)
    print(Farbe['Grün_Fettdruck'] + _('Extraktion abgeschlossen') + Farbe['Weiß_Fettdruck'] + '\n' + _("Verarbeite {0} Einträge").format(Eintragszähler) + Farbe['Weiß_Normaldruck'] + '\n' + _("Schreibe .strings-Dateien für {0} Sprachen".format(len(Sprachliste))))
    Stringsdateien_schreiben()
    print(Farbe['Weiß_Fettdruck'] + _('Übertrage Dateien zu Transifex') + Farbe['Weiß_Normaldruck'] + '\n' + _('Verbinde zu Transifex…'))
    # Übertrage vorbereitete Stringsdateien zu Transifex (transfer prepared strings files to transifex)
    if not Nicht_Senden:
        Transifex_Stringsquelle_senden()
        Transifex_Stringsdateien_senden()
    else:
        print("    " + _("Keine Übertragung, Option -HL wurde nicht angegeben"))

elif Optionen.get('Aktualisierungsmodus'):
    print('\n' + Farbe['Blau_Fettdruck'] + '-------------------------\n* ' + _("Erzeuge Sicherungskopie") + '\n-------------------------\n' + Farbe['Weiß_Fettdruck'] + _("Verarbeite {0} .desktop-Dateien").format(len(Desktopdateiliste)) + Farbe['Weiß_Normaldruck'])
    Desktop_Sicherungskopie()
    # Einlesen der Sprachen aus der Sprachdatei in eine globale Liste (read locale file content into global locale list)
    Sprachdatei_lesen()
    print(Farbe['Weiß_Fettdruck'] + _("Verarbeite {0} Sprachen").format(len(Sprachliste)) + Farbe['Weiß_Normaldruck'] + "\n · {0} ".format('Sprache:') + "\n · {0} ".format('Sprache:').join(Sprachliste) +
    '\n\n' + Farbe['Blau_Fettdruck'] + '--------------------------------\n* ' + _("Aktualisiere .desktop-Dateien:") + '\n--------------------------------' + Farbe['Weiß_Normaldruck'])
    if not Lokal:
        print(Farbe['Weiß_Fettdruck'] + _('Hole .strings-Quelldateien vom Server') + Farbe['Weiß_Normaldruck'] + '\n' + _('Verbinde zu Transifex…'))
        # Hole übersetzte Stringsdateien von Transifex (fetch translated strings files from transifex)
        Transifex_Stringsquelle_holen()
        Transifex_Stringsdateien_holen()
    else:
        print(Farbe['Weiß_Fettdruck'] + _('Hole lokal bereitgestellte .strings-Dateien') + Farbe['Weiß_Normaldruck'] + '\n' + _('Kopiere…'))
        # Kopiere lokale Stringsdateien aus dem angegebenen Quellordner (copy strings files from local source folder)
        Lokale_Stringsdateien_holen(Lokales_Verzeichnis)
        Lokale_Quelldatei_holen(Lokales_Verzeichnis)
    # Prüfen, ob Stringsdateien für die weitere Verarbeitung vorhanden sind (check whether there are strings files for processing)
    Voraussetzungen_02()
    # Übertrage Inhalt der übersetzten Stringsdateien in ein globales Wörterbuch (read strings files content into global dictionary)
    Stringsdateien_lesen()
    if Eintragszähler == 0:
        print(Farbe['Rot_Fettdruck'] + _("Fehler: Verarbeitung fehlgeschlagen: Keine übersetzten Einträge vorhanden.") + Farbe['Weiß_Normaldruck'])
        exit(1)
    print(Farbe['Weiß_Fettdruck'] + _("Verarbeite {1} .strings Datei(en), {0} Einträge").format(Eintragszähler,len([x for x in os.listdir(Stringspfad) if os.path.isfile(os.path.join(Stringspfad, x)) and x.endswith('.strings')])) + Farbe['Weiß_Normaldruck'] + '\n' + "Schreibe .desktop-Dateien für {0} Programme:".format(len(Desktopdateiliste)) + Farbe['Weiß_Normaldruck'])
    # Eintragen der Übersetzungen aus dem Wörterbuch in die Desktopdateien aus dem Quellverzeichnis, speichern im Zielverzeichnis (parse all resources present in Desktop_Quellpfad directory)
    for Aktuelle_Ressource in Desktopdateiliste:
        Desktopdatei_schreiben("/{0}.desktop".format(Aktuelle_Ressource), parse("\n".join(Desktopdatei_lesen("/{0}.desktop".format(Aktuelle_Ressource))), Aktuelle_Ressource))
    print(Farbe['Grün_Fettdruck'] + _('Aktualisierung abgeschlossen') + '\n' + Farbe['Weiß_Normaldruck'])
    # Aus den aktualisierten Desktopdateien ein neues .deb installationspaket Paket bauen. (prepare and build a deb installer package from the actualised .desktop files)
    Paket_vorbereiten()
    print(Farbe['Grün_Fettdruck'] + _('Paketvorbereitung abgeschlossen') + '\n' + Farbe['Weiß_Normaldruck'])
    if Optionen.get('Gitlab'):
        print('\n' + Farbe['Blau_Fettdruck'] + '-------------------------\n* ' + _("Übertragung an Gitlab") + '\n-------------------------\n' + Farbe['Weiß_Normaldruck'])
        Git_Vorbereiten()
        if not Nicht_Senden:
            Git_Senden()
        else:
            print("    " + _("Keine Übertragung, Option -HL wurde nicht angegeben"))

elif Optionen.get('Quellaktualisierungsmodus'):
    print('\n' + Farbe['Blau_Fettdruck'] + '-------------------------\n* ' + _("Erzeuge Sicherungskopie") + '\n-------------------------\n' + Farbe['Weiß_Normaldruck'])
    TX_Sicherungskopie()
    Desktop_Sicherungskopie()
    Sprachdatei_lesen()
    print(Farbe['Grün_Fettdruck'] + _('Sicherung abgeschlossen') + '\n' + Farbe['Weiß_Normaldruck'])
    # Einlesen aller übersetzbaren Einträge aus den Desktopdateien in das Hauptwörterbuch (read all translatable desktop files strings into main dictionary)
    print('\n' + Farbe['Blau_Fettdruck'] + '----------------------------------\n* ' + _("Extrahiere übersetzbare Einträge") + '\n----------------------------------\n' + Farbe['Weiß_Fettdruck'] + _("Verarbeite {0} .desktop-Dateien").format(len(Desktopdateiliste)) + Farbe['Weiß_Normaldruck'])
    Eintragszähler = 0
    Sprachliste.append(Projektsprache) #(we want to process the source strings file the same way, so Quellstrings_lesen is deprecated now)
    for Aktuelle_Ressource in Desktopdateiliste:
        parse("\n".join(Desktopdatei_lesen("/{0}.desktop".format(Aktuelle_Ressource))), Aktuelle_Ressource)
    print(Farbe['Grün_Fettdruck'] + _('Extraktion abgeschlossen') + Farbe['Weiß_Fettdruck'] + '\n' + _("Verarbeite {0} Einträge").format(Eintragszähler) + Farbe['Weiß_Normaldruck'] + '\n' + _("Schreibe .strings-Dateien für {0} Sprachen".format(len(Sprachliste))))
    Sprachliste = [Projektsprache] #(we want to process the source strings file the same way, so Quellstrings_lesen is deprecated now)
    Stringsdateien_schreiben() # Nur Quellstrings schreiben, alle anderen Einträge erst nach Aktualisierung des Hauptwörterbuchs mit den Übersetzungen von Transifex schreiben.
    # Einlesen der Sprachen aus der Sprachdatei in eine globale Liste (read locale file content into global locale list)
    Sprachdatei_lesen()
    print(Farbe['Weiß_Fettdruck'] + _("Verarbeite {0} Sprachen").format(len(Sprachliste)) + Farbe['Weiß_Normaldruck'] + "\n · {0} ".format('Sprache:') + "\n · {0} ".format('Sprache:').join(Sprachliste) +
    '\n\n' + Farbe['Blau_Fettdruck'] + '---------------------------------------------\n* ' + _("Aktualisiere mit vorhandenen Übersetzungen:") + '\n---------------------------------------------' + Farbe['Weiß_Normaldruck'])
    print(Farbe['Weiß_Fettdruck'] + _('Hole .strings-Quelldateien vom Server') + Farbe['Weiß_Normaldruck'] + '\n' + _('Verbinde zu Transifex…'))
    # Hole übersetzte Stringsdateien von Transifex (fetch translated strings files from transifex)
    Lokale_Stringsdateien_holen(Sicherungspfad)
    # Prüfen, ob Stringsdateien für die weitere Verarbeitung vorhanden sind (check whether there are strings files for processing)
    Voraussetzungen_02()
    print(Farbe['Weiß_Fettdruck'] + _("Verarbeite {0} .strings Datei(en)").format(len([x for x in os.listdir(Stringspfad) if os.path.isfile(os.path.join(Stringspfad, x)) and x.endswith('.strings')])) + Farbe['Weiß_Normaldruck'])
    # Übertrage Inhalt der übersetzten Stringsdateien in ein globales Wörterbuch (read strings files content into global dictionary)
    Stringsdateien_lesen()
    if Eintragszähler == 0:
        print(Farbe['Rot_Fettdruck'] + _("Fehler: Verarbeitung fehlgeschlagen: Keine übersetzten Einträge vorhanden.") + Farbe['Weiß_Normaldruck'])
        exit(1)
    print(_("Schreibe .strings-Dateien für {0} Sprachen".format(len(Sprachliste))) + '\n')
    Stringsdateien_schreiben() # Alle Einträge aus dem Hauptwörterbuch in die lokalisierten Stringsdateien übertragen.
    print(_("Schreibe .desktop-Dateien für {0} Programme:".format(len(Desktopdateiliste))) + Farbe['Weiß_Normaldruck'])
    # Eintragen der Übersetzungen aus dem Wörterbuch in die Desktopdateien aus dem Quellverzeichnis, speichern im Zielverzeichnis (parse all resources present in Desktop_Quellpfad directory)
    for Aktuelle_Ressource in Desktopdateiliste:
        Desktopdatei_schreiben("/{0}.desktop".format(Aktuelle_Ressource), parse("\n".join(Desktopdatei_lesen("/{0}.desktop".format(Aktuelle_Ressource))), Aktuelle_Ressource))
    print(Farbe['Grün_Fettdruck'] + _('Aktualisierung abgeschlossen') + '\n' + Farbe['Weiß_Normaldruck'])
    if not Nicht_Senden:
        Transifex_Stringsquelle_senden()
        Transifex_Stringsdateien_senden()
    else:
        print("    " + _("Keine Übertragung, Option -HL wurde nicht angegeben"))
    # Aus den aktualisierten Desktopdateien ein neues .deb installationspaket Paket bauen. (prepare and build a deb installer package from the actualised .desktop files)
    print(Farbe['Blau_Fettdruck'] + '-----------------\n* ' + _("Erstelle Paket:") + '\n-----------------' + Farbe['Weiß_Normaldruck'])
    Paket_vorbereiten()
    print(Farbe['Grün_Fettdruck'] + _('Paketvorbereitung abgeschlossen') + '\n' + Farbe['Weiß_Normaldruck'])
    if Optionen.get('Gitlab'):
        print('\n' + Farbe['Blau_Fettdruck'] + '-------------------------\n* ' + _("Übertragung an Gitlab") + '\n-------------------------\n' + Farbe['Weiß_Normaldruck'])
        Git_Vorbereiten()
        if not Nicht_Senden:
            Git_Senden()
        else:
            print("    " + _("Keine Übertragung, Option -HL wurde nicht angegeben"))

elif Optionen.get('Übersetzungsmodus'):
    print('\n' + Farbe['Blau_Fettdruck'] + '-------------------------\n* ' + _("Erzeuge Sicherungskopie") + '\n-------------------------\n' + Farbe['Weiß_Normaldruck'])
    TX_Sicherungskopie()
    Desktop_Sicherungskopie()
    # Einlesen der Sprachen aus der Sprachdatei in eine globale Liste (read locale file content into global locale list)
    Sprachdatei_lesen()
    # Originally designed to process all languages in one go, it was changed to process languages one by one to minimise risk of overwriting manual translations on transifex which have been added between download and reupload of a language.
    Haupt_Sprachliste = Sprachliste
    if not Lokal:
        print('Verbinde zu Transifex…')
        Transifex_Stringsquelle_holen()        
    else:
        Lokale_Quelldatei_holen(Lokales_Verzeichnis)
    Sprachliste = [Projektsprache] #(we want to process the source strings file the same way, so Quellstrings_lesen is deprecated now)
    Stringsdateien_lesen()
    print(_("Verarbeite .strings-Datei, {0} Einträge").format(Eintragszähler))
    for Aktuelle_Sprache in Haupt_Sprachliste:
        Sprachliste = [Aktuelle_Sprache]
        Eintragszähler = 0
        print('\n' + Farbe['Blau_Fettdruck'] + "--------------------\n* Übersetze zu {0}\n--------------------".format(Aktuelle_Sprache) + Farbe['Weiß_Normaldruck'])
        if not Lokal:
            # Hole unübersetzte und nur teilweise übersetzte Stringsdateien von Transifex (fetch partly translated and untranslated strings files from transifex)
            print('Verbinde zu Transifex…')
            Transifex_Stringsdateien_holen()
        else:
            print(Farbe['Weiß_Fettdruck'] + _('Hole lokal bereitgestellte Datei') + Farbe['Weiß_Normaldruck'] + '\n' + _('kopiere...'))
            # Kopiere lokale Stringsdatei aus dem Quellordner (copy strings files from local source folder)
            Lokale_Stringsdatei_holen(Lokales_Verzeichnis)
        Stringsdateien_lesen()
        print(_("Verarbeite .strings-Datei, {0} Einträge").format(Eintragszähler) + '\n')
        Hauptwörterbuch_übersetzen()
        print(Farbe['Grün_Fettdruck'] + _('Übersetzung zu {0} abgeschlossen.').format(Aktuelle_Sprache) + Farbe['Weiß_Normaldruck'])
        print("\n" + _("Schreibe .strings-Datei"))
        Stringsdateien_schreiben()
# Ergänzen: .strings-Datei vor dem Übertragen nochmals von Transifex holen, mit vorheriger vergleichen um sicherzustellen, daß auf Transifex keine manuellen Änderungen an der Übersetzung während der Verarbeitung vorgenommen wurden. (Todo: Redownload from transifex and compare checksums to make sure nobody has modified the translation on transifex while automatic translation was running)
        print(Farbe['Weiß_Fettdruck'] + _('Übertrage Dateien zu Transifex') + Farbe['Weiß_Normaldruck'] + '\n' + _('Verbinde zu Transifex…'))
        # Übersetzte .strings-datei zu Transifex übertragen (Upload translated language strings file back to transifex)
        if not Nicht_Senden:
            Transifex_Stringsdateien_senden()
        else:
            print("    " + _("Keine Übertragung, Option -HL wurde nicht angegeben"))
    print("\n" + Farbe['Blau_Fettdruck'] + "--------------------\n* " + _("Schreibe .desktop-Dateien für {0} Programme:").format(len(Desktopdateiliste)) + "\n--------------------" + Farbe['Weiß_Normaldruck'])
    for Aktuelle_Ressource in Desktopdateiliste:
        Desktopdatei_schreiben("/{0}.desktop".format(Aktuelle_Ressource), parse("\n".join(Desktopdatei_lesen("/{0}.desktop".format(Aktuelle_Ressource))), Aktuelle_Ressource))
    # Aus den aktualisierten Desktopdateien ein neues .deb installationspaket Paket bauen. (prepare and build a deb installer package from the actualised .desktop files)
    Paket_vorbereiten()
    print(Farbe['Grün_Fettdruck'] + _('Paketvorbereitung abgeschlossen') + '\n' + Farbe['Weiß_Normaldruck'])
    if Optionen.get('Gitlab'):
        print('\n' + Farbe['Blau_Fettdruck'] + '-------------------------\n* ' + _("Übertragung an Gitlab") + '\n-------------------------\n' + Farbe['Weiß_Normaldruck'])
        Git_Vorbereiten()
        if not Nicht_Senden:
            Git_Senden()
        else:
            print("    " + _("Keine Übertragung, Option -HL wurde nicht angegeben"))

elif Optionen.get('Sicherungsmodus'):
    print('\n' + Farbe['Blau_Fettdruck'] + '-------------------------\n* ' + _("Erzeuge Sicherungskopie") + '\n-------------------------\n' + Farbe['Weiß_Normaldruck'])
    TX_Sicherungskopie()
    Desktop_Sicherungskopie()
    if Optionen.get('Gitlab'):
        Git_Sicherungskopie()
    print(Farbe['Grün_Fettdruck'] + _('Sicherung abgeschlossen') + '\n' + Farbe['Weiß_Normaldruck'])

exit(0)
