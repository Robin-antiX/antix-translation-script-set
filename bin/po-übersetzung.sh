#!/bin/bash

# License: GPL v.3
# Version 0.68
# Written  (C) 2021 by Robin (for antiX community)
# Tiny script to (semi-)automatically translate our .pot files
# it extracts all content of msgid-strings from a given .pot file, sends it string
# by string to an online translation service for all languages found in list and
# saves the translation back to the msgstr-strings in the respective .po file.
# All the .po files created are stored in LC_MESSAGES subfolder of target language
# in a base destination directory. Finally .mo files are compiled.
# 1/2023 updated to work with new transifex client.

# Needs »translate-shell« installed.
# needs recent transifex client from https://github.com/transifex/cli/releases (don't use outdated python-script version from debian repos!)

# Hint: this script was written in German language originally, but it has been
# translated to English meanwhile. Use English language .mo file attached to have
# it run it in English language. Simply copy this .mo file to your
#    '/usr/share/locale/en/LC_MESSAGES'
# folder before starting the script.

# todo: check whether push and pull to tranisfex was successful, otherwise wait and retry (like translation service already does.)
# todo: update usage instructions for gitlab and automatic preparation of .pot file for python brace format
# todo: create localisation files for some other languages (fr, pt_BR, el, it etc.)

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=po-übersetzung.sh


# Workflow and Instructions:
# --------------------------
#
# 1.) Install and set up transifex client, so you can use tx pull and push commands non-interactively.
#     (This means to have a file .transifexrc present in your home folder, and the tx client can be invoked
#     from command line by entering the command tx)
#
# 2.) Add all languages you want to process to your personal transifex profile so you can
#     access them for edits in transifex translation editor.
#
# 3.) Create a working base folder and put this script into this folder. Local subfolder
#     structure will be created automatically on first use. (Always start the script from within
#     this parent folder in order to allow relative paths to match.)

# Foreign language users may want to edit the configurable variables at the beginning of the script.
# You can translate the file name of sprachen.lst to your language and use the changed filename then.
# Also the expected prefix BEARBEITET can get translated or changed to whatever you like this way. If
# you want to translate the script name itself from po-übersetzen.sh to your language you may want to
# edit the textdomain entry above accordingly, and make sure the .mo file in use matches the new name.

# 4.) Copy the sprachen.lst file into the very same working base folder where the script itself is
# located. Make sure the active languages in this file match what langauges are present in the project
# at transifex. Comment out or remove all languages not wanted to be translated.

# The sprachen.lst file is a simple .txt file containing the identifiers of all languages you want to have a translation
# to (which means: all languages present for your project at transifex). One entry per line, nothing else in this line,
# e.g. no leading or trailing blanks. A sharp sign # in front of a language identifer can be used to exclude it from
# processing without deleting it. Languages marked this way will be ignored.
# For the language identifiers you'll have to use the gettext/linux style or syntax, e.g. fr, en, fil ... in small
# letters for two/three letter code, and zh_TW, pt_BR or fr_BE in four/five significant letter code, the first pair has
# to be in small, the second in big letters. It will be reformated by the script to match the requirements of online
# translation service, (which means: underline characters are replaced by minus sign) before tansmission to the service.
# In case your translation service has different needs, change the respective formating in translation subroutine.

# 5.) Upload the most recent, updated .pot file for translation to transifex. Then you need to write
#     down the following pieces of information from the transifex site:
#        transifex organisation "slug"  (e.g. "antix-linux-community-contributions")
#        transifex project "slug"  (e.g. "anitx-contribs")
#        transifex resource "slug"  (e.g. unplugdrive)
#        transifex resource language (e.g. en@pirate)
#    check out what is the true source language of the resource to be translated from (e.g. french,
#    italian, chinese or whatever)
#
# 6.) Start script for the first time, using the needed parameters on command line, e.g.:
#     "/bin/bash po-übersetzung.sh antix-linux-community-contributions anitx-contribs en@pirate unplugdrive en"
#     The command line options of po-übersetzen are expected in the following order:
#     "transifex-organisation-slug transifex-project-slug transifex-project-language transifex-resource-slug true-resource-language"
#
# The script will fetch the most recent .pot file from transifex and create the needed subfolder structure
# inside the base folder from where you have started the script.
# You can run the script for multiple projects and resources of an "organisation" from within the same working base
# folder, each project will get its own private local project subfolder.
#
# 7.) Create a copy of the downloaded .pot file as translation pattern, name this copy to "BEARBEITET.unplugdrive.pot"
#     and reformat it as follows:
#     - split all msgid-strings with untranslatable parts in it, so all translatable parts sit on separate lines.
#     - mark all untranslatable lines by the small letter x at the very beginning of the line.
#     Hint: If the .pot file doesn't contain any untranslatable parts just copy the file 1:1 to the expected naming
#     (original name preceeded by the expected prefix, e.g. BEARBEITET.<original filename> )
#
# Example:
#
# the entry
#
#        msgid "  Usage: unplugdrive.sh [options]"
#
# has to be split up to
#
#        msgid ""
#        x"  "
#        "Usage:"
#        x" unplugdrive.sh ["
#        "options"
#        x"]"
#
# While reformatting make absolutely sure not to modify, remove or add any characters to the msgid strings in
# between the double quotes. Otherwise gettext won't be able to recognise the msgid later. You may split as often
# as you like, even single characters or text formatting tags, which wouldn't survive the online translation
# process. But be aware, that a translation will be the better and more precisely, the more complete the
# sentence for translation is. From the single word "on" the translator might not guess correctly whether
# it is meant as "on/off" contradistinction or whether it means "Peter stands on top of his chair.", which makes an
# important difference in many languages in terms of understandig the meaning, in languages which have different
# words for this.
#

# 8.) Restart script for processing:
# Please be carefully, no checks are done whether a directory or a file exists. So make sure nothing important
# is in your way and gets overwritten while running this script.
#
# You can set the $dry_run flag to "true" in order to deactivate real translations and upload after processing. Script
# will process files using a dummy translation instead, and you can check the result in locale folder before
# starting the real run. In dry run languages are not detectable as processed, so no resume possible in this test mode.
#
# The actual processing is started by calling the script with the very same command and command line parameters
# as you've used on the first run for this resource. Just repeat the command after having provided the
# translation pattern file.
# 
# You may run the script unattended, but check from time to time whether the translation service provider didn't block
# your IP and doesn't respond anymore. The script will retry in increasing intervals and repeat untily you have got a
# fresh IP. It will either resume automatically or by you pressing w key. If this blocking happens to you regularly,
# you might want to chose a different timing formula, there are some examples below, of which only one should get
# activated at a time. In case only your internet connection gets lost while processing, simply reestablish a
# connection. Script will pause and resume automatically as soon connection is back. No need to stop processing.
# You may hibernate the machine while processing without stopping, it will resume with next line. A real system shut
# down instead will render the last processed language incomplete, but script will translate this language anew from
# scratch when restarting it.

# Only empty translation entries are processed, existing translations in a .po file will not get overwritten.

# After translation was completed the script redownloads the translation from transifex first and compares it with
# the file downloaded before translation for this language had started. The translation result will only get uploaded
# to transifex if there is no difference, which means nobody had added or modified translations on transifex meanwhile.
# Attention: There still is a slight chance some very fresh translations get lost in case somebody added them on transifex
# in the very seconds between check downloading and uploading the .po file.

# The script outputs .mo files and .po files both, each language in git subdirectories below the
# output directory you have set. The .mo files are ready to use in any linux system, and the .po files are used
# for upload to transifex, so other translators may improve/correct them manually in their respective language.
# Keep in mind: What you get here is machine translation only (though meanwhile on a pretty high standard).

# Script nices itself to 14 to make it run with lower priority, so it wouldn't disturb your normal work at pc while
# running. In case you want it to have full proirity, deactivate the respective line in script or change the value.

# Considerations for time consumption: Translation of unplugdrive.sh.pot to about 100 languages took 36 hours.
# The time needed dependes on how much separate stings are to be sent to the translation services and the timing
# formula you can use.


# --------------------Konfigurierbare Variablen (Configurabele variables) -------------------------
prefix="BEARBEITET"                 # This is the prefix of your specially crafted/reformatted pot file. Has also to sit in folder you've set in "ausgabeverzeichnis" as base folder.
sprachenliste="sprachen.lst"        # list of language identifiers, one entry per line, 2 letter code allowed (and some 4 significant letter also, depends on translation service used).
# -------------------------------------------------------------------------------------------------


if [ -z "$1" ]; then
    echo $"Verwendung: po-übersetzung.sh <transifex-Organisationskennung> <transifex-Projektkennung> <transifex-Ressourcen-Sprachbezeichnung> <transifex-Ressourcenkennung> <Wahre-Ressourcensprache> [dryrun]
Nur der letzte Parameter (dryrun) ist optional, die Reihenfolge ist einzuhalten.
"
    exit 1
fi

function korrigiere_zeilenenden {
# fix line ends
    sed 's/\r$//' "$1" | tr '\r' '\n' > "$1-$$"
    mv "$1-$$" "$1"
}

dry_run=false
organisationskennung="$1"
projektkennung="$2"
transifex_ressourcenbezeichnung="$3"
ressourcenkennung="$4"
ausgangssprache="$5"
if [ -n "$6" ]; then [ "$6" == "dryrun" ] && dry_run=true ; fi

uebertragungsfehler=""

# create base structure for ressource
pot_datei="$ressourcenkennung.pot"
ausgabeverzeichnis="./po"
localeverzeichnis="./locale"
gitverzeichnis="./git"
mkdir -p "./$projektkennung/$ressourcenkennung/.tx"
# create resource specific tx configuration file
echo "[main]
host = https://www.transifex.com

[o:$organisationskennung:p:$projektkennung:r:$ressourcenkennung]
file_filter = po/<lang>/$ressourcenkennung.po
minimum_perc = 0
source_file = po/$pot_datei
source_lang = $transifex_ressourcenbezeichnung
type = PO
" > "$projektkennung/$ressourcenkennung/.tx/config"

# run all following commands from within transifex working directory to make tx client happy.
arbeitsverzeichnis="$(pwd)"
cd  "./$projektkennung/$ressourcenkennung"

if ! [ -s "./po/$pot_datei" ]; then
echo $"Keine .pot Datei gefunden. Hole aktuelle .pot-Datei vom transifex Server"
    tx pull -s -r "$projektkennung.$ressourcenkennung" # --no-interactive
    korrigiere_zeilenenden "./po/$pot_datei"  # replace possibly existing invalid line ends from different os standards in file
    sed -i '/msgstr/,/^$/{H;$!d};x;/msgid ""/b;s/msgstr..*/msgstr ""\n/' "./po/$pot_datei"    # remove invalid translation entries added by transifex from pot file
fi
if ! [ -s "./po/$prefix.$pot_datei" ]; then
    mkdir -p "$gitverzeichnis/$ressourcenkennung/po"
    cp "$ausgabeverzeichnis/$pot_datei" "$gitverzeichnis/$ressourcenkennung/po"
    echo $"Kein Übersetzungsmuster gefunden. Bitte erstellen Sie die Datei
  ./$projektkennung/$ressourcenkennung/po/$prefix.$pot_datei
aus der original .pot Datei und starten das Script erneut."
    exit 1
fi


renice -n 14 -p $$  # let's work in background

temporaerdatei="/tmp/po-übersetzung-$$-"
function aufraeumen {
# cleanup on exit
    rm -f /tmp/po-übersetzung-$$-*.tmp
    [ -n "$uebertragungsfehler" ] && echo -e "\n \033[1;31m"'!!! '$"Fehler bei der Übertragung zu Transifex""$uebertragungsfehler""\033[0;37m"
}
trap aufraeumen EXIT

function populategit {
# distribute results to proper output folders for further processing.
        echo $"Erzeuge Dateien im git-Verzeichnis."
        mkdir -p "$gitverzeichnis/$ressourcenkennung/po/$zielsprache"
        [ -e "$ausgabedatei" ] && mv "$ausgabedatei" "$ausgabedatei.bak3"
        sleep 2  #wait for server to sync the uploaded file before pulling again
        tx pull -f -l "$zielsprache" -r "$projektkennung.$ressourcenkennung" # --no-interactive
        # erzeuge .mo-datei
        # msgfmt -o "$(echo $ausgabedatei | rev | cut -d. -f2- | rev ).mo" "$ausgabedatei"
        mkdir -p "$localeverzeichnis/$zielsprache/LC_MESSAGES"
        msgfmt -o "$localeverzeichnis/$zielsprache/LC_MESSAGES/$(echo "$pot_datei" | rev | cut -d. -f2- | rev).mo" "$ausgabedatei"
        mkdir -p "$gitverzeichnis/$ressourcenkennung/locale/$zielsprache/LC_MESSAGES"
        cp "$localeverzeichnis/$zielsprache/LC_MESSAGES/$(echo "$pot_datei" | rev | cut -d. -f2- | rev).mo" "$gitverzeichnis/$ressourcenkennung/locale/$zielsprache/LC_MESSAGES"
        mv "$ausgabedatei" "$gitverzeichnis/$ressourcenkennung/po/$zielsprache"
}

function nachbearbeiten {
# aftermath
        # reformat classic apple style newlines to unix (We had to use \r instead of \n in string processing, since \n wouldn't work for some reason.)
        sed 's/\r$//' "$ausgabedatei" | tr '\r' '\n' > "$ausgabedatei-$$"
        mv "$ausgabedatei-$$" "$ausgabedatei"
        # entferne doppelte Leerzeile am Dateiende (remove double empty line at file end)
        sed -i '${/^$/d;}' "$ausgabedatei"
        # ergänze Kopfeinträge
        sed -i "s/^\"Language: \\\n\"$/\"Language: $zielsprache \\\n\"/" "$ausgabedatei"
        sed -i "s/^\"PO-Revision-Date: ..*\\\n\"$/$(date "+\"PO-Revision-Date: %F %R%z \\\n\"")/" "$ausgabedatei"

        sed -i.bak2 's/51a2/{0}/g;s/51a3/{1}/g;s/51a4/{2}/g' "$ausgabedatei"
}

function hochladen {
# upload to transifex
        # Prüfen ob Übersetzung auf Transifex zwischenzeitlich verändert wurde. (check whether translation has changed on transifex)
        mv "$ausgabedatei" "$ausgabedatei.bak4"
        tx pull -f -l "$zielsprache" -r "$projektkennung.$ressourcenkennung" # --no-interactive
        version_01="$(shasum -a1 $ausgabedatei.bak | cut -d' ' -f1)"
        version_02="$(shasum -a1 $ausgabedatei | cut -d' ' -f1)"
        if [ "$version_01" == "$version_02" ]; then
            rm -f "$ausgabedatei"
            mv "$ausgabedatei.bak4" "$ausgabedatei"
            sleep 2  #wait for server to allow pushing again
            # .po-Datei zum transifex server hochladen (transfer translated .po file back to transifex)
            echo -e $"\nÜbertrage komplettierte Übersetzung zum transifex Server"
            tx push -l "$zielsprache" -r "$projektkennung.$ressourcenkennung" -t # --no-interactive
            # todo: Error check and stop when transifex server is down or not reachable
        else
            mv "$ausgabedatei" "$ausgabedatei.versionskonflikt"
            mv "$ausgabedatei.bak4" "$ausgabedatei"
            echo -e "\n \033[1;31m"'!!! '$"Übertragung zum Tranisfex-Server wegen Versionskonflikt nicht möglich.""\n"' !!! '$"Übersetzung auf Transifex wurde zwischenzeitlich verändert.""\033[0;37m"
            uebertragungsfehler="$uebertragungsfehler"", $zielsprache"
        fi
}

#function gitupload {
#    not implemented yet
#}


function uebersetzen {
# translate
                        k=30
                        ist_nichtuebersetzt=true
                        while $ist_nichtuebersetzt; do
                            ist_nichtuebersetzt=false
                            if $dry_run; then
                                uebersetzung="Übersetzt: $uebersetzungszeile" # (placeholder string for dry run)
                            else
                                # prüfe, ob der Übersetzerdienst noch erreichbar ist (check whether internet connection is still present)
                                while ! nc -zw1 8.8.8.8 443; do
                                    sleep 30            # wait gracefully in case internet connection is broken for some time
                                done
                                # Entferne Zeilenumbrüche, welche von Googles KI fehlerhaft interpretiert werden, z.B. als das Wort "not". (remove \\n and \\t from translation strings) Diese müssen von Übersetzern auf transifex später wieder manuell ergänzt werden.
                                uebersetzungszeile="$(echo $uebersetzungszeile | sed 's/\\\\n/ /g;s/\\\\t/ /g')"
                                # Schicke die Zeichenkette an den Übersetzungsdienst (send string for translation to translation service)
                                uebersetzung="$(trans -s $(echo "$ausgangssprache" | tr '_' '-') -t $(echo "$zielsprache" | tr '_' '-') -b --show-original n --show-languages n --show-original-dictionary n \
--show-dictionary n --show-alternatives n --show-prompt-message n --show-original-phonetics n --show-translation-phonetics n \
--show-translation y -- "$uebersetzungszeile")"
                            fi
                            # Prüfe Ergebnis

                            if [ "$uebersetzung" == "" ]; then
                                ist_nichtuebersetzt=true;
                                echo -e "\n \033[1;31m"'!!! '$"Der Übersetzungsdienst antwortet nicht.""\n \033[0;31m"$"In $k Sekunden wird der Versuch automatisch widerholt. Falls der Dienst\nweiterhin nicht antwortet, versuchen eine andere IP Adresse zu erhalten\nund drücken anschließend die Taste »w«. Falls sie die Übersetzung\nabbrechen möchten drücken Sie zum Beenden die Taste »b«.""\033[0;37m"
                                        while :; do
                                            read -rsn1 -t $k entscheidung
                                            if [ "$entscheidung" = "b" ] || [ "$entscheidung" = "B" ]; then
                                                # Übermitteln der angefangenen Übersetzung an Transifex. (let's try to upload to transifex what we have already translated before exiting.)
                                                # Abschneiden des unvollständigen letzten Eintrags (cut the broken last entry from the file:)
                                                letzte_zeile="$(sed -n '/^msgid "/=;$!d' $ausgabedatei | sed -n '$p')"
                                                letzte_zeile=$((letzte_zeile - 1))
                                                sed -i.err -n '1,'"$letzte_zeile"'p' "$ausgabedatei"
                                                tac "$ausgabedatei" | sed ':x;N;/^#..*$/d;Tx;{:y;n;p;ty}' | tac > "$ausgabedatei".tmp
                                                mv "$ausgabedatei".tmp "$ausgabedatei"
                                                nachbearbeiten
                                                hochladen
                                                echo -e "*  "$"Die Übersetzung wurde beendet.""\n*  \033[1;37m"$"Zum Fortsetzen mit dieser Sprache Skript erneut starten.""\033[0;37m"
                                                rm -rf "$ausgabeverzeichnis/$zielsprache"  # remove partly translated target language on break of processing.
                                                exit 0
                                            elif [ "$entscheidung" = "w" ] || [ "$entscheidung" = "W" ]; then
                                                k=30
                                                break
                                            elif [ "$entscheidung" = "" ]; then
                                                [ $k -lt 600 ] && k=$((k+30))
                                                break
                                            fi
                                        done
                            else
                                k=30
                            fi
                        done
                        # Ersetze alle vom Übersetzungsdienst eingesetzten unmaskierten doppelten Anführungszeichen und breitenlosen Leerzeichen in der Zeichenkette. (Replace invalid character substitutions coming back from translation service)
                        uebersetzung="$(echo $uebersetzung| sed "s/\([[:blank:][:punct:]][[:blank:][:punct:]]*\)\"\([[:digit:][:alnum:]][[:digit:][:alnum:]]*\)/\1»\2/g;s/\([[:digit:][:alnum:]]*\)\"\([[:blank:][:punct:]][[:blank:][:punct:]]*\)/\1«\2/g;s/\"/“/g;s/u2000/ /g;s/u2001/ /g;s/u2002/ /g;s/u2003/ /g;s/u2004/ /g;s/u2005/ /g;s/u2006/ /g;s/u2007/ /g;s/u2008/ /g;s/u2009/ /g;s/u200a/ /g;s/u200b/​/g;s/u200c/‌/g;s/u200d/‍/g;s/u200e/‎/g;s/u200f/‏/g;s/u003d/=/g;s/u003c/</g;s/u003e/>/g")" # beware: all the replacements are NOT default blanks (u0020) or empty strings, but actually the respective utf space and formatting characters
                        # Ersetze alle vom Übersetzungsdienst beschädigtne typographischen Auszeichnungen (restore <i>, </i>, <u>, </u>, <b>, </b>)
                        uebersetzung="$(echo $uebersetzung | sed 's/< i>/<i>/g;s/<i >/<i>/g;s/< \/i>/<\/i>/g;s/<\/i >/<\/i>/g; s/<\/ i>/<\/i>/g')"
                        uebersetzung="$(echo $uebersetzung | sed 's/< u>/<u>/g;s/<u >/<u>/g;s/< \/u>/<\/u>/g;s/<\/u >/<\/u>/g; s/<\/ u>/<\/u>/g')"
                        uebersetzung="$(echo $uebersetzung | sed 's/< b>/<b>/g;s/<b >/<i>/g;s/< \/b>/<\/b>/g;s/<\/b >/<\/b>/g; s/<\/ b>/<\/b>/g')"
                        # Entferne alles vom Übersetzungsdienst unkenntlich gemachten Zeilenumbrüche und Einzüge (remove all returned broken \\n \\t)
                        uebersetzung="$(echo $uebersetzung | sed -r 's/\\\\([^t^n])/\1/g')"
                        # Ersetze einfache Anführungszeichen/Apostrophe durch UTF (replace simple single quotes by respective UTF character)
                        uebersetzung="$(echo $uebersetzung | sed 's/'"'"'/’/g')"
#                       echo "$uebersetzung" >> "$textdatei"                        # schreibe übersetzte Zeile in Textdatei (Write translated line to additional textfile)
                        if ! $dry_run; then
                            # sleep $(($(echo ${RANDOM:0-1} | sed 's/^ /0/')*11+21))    # use this to prevent the translation service from blocking your IP for
                                                                                        # presumed flooding attrack. Value range generated is ~20 sec. to ~2 min.
                            # sleep $(($(echo ${RANDOM:0-2} | sed 's/^0//')*2+31))      # other formula, will generate values between ~30 seconds and ~4,5 minutes
                                                                                        # in case formula above doesn't prevent you from being blocked.
                            #sleep $(($(echo ${RANDOM:0-1} | sed 's/^ /0/')*2+5))       # fast formula in case your IP isn't not blocked generating values between 5 sec. and ~20 sec.
                            sleep $((($(echo ${RANDOM:0-1} | sed 's/^ /0/')+11)/2))     # faster formula for 5 to 10 sec.
                            # sleep $(($(echo ${RANDOM:0-1} | sed 's/^ /0/')+1))        # super fast formula generating values between 1 sec. and 10 sec.
                        fi
}

# msgid Einträge aus .pot-Datei extrahieren und in nummerierte temporärdateien schreiben. Eintragsnummer=Dateinummer. (Extract msgid entries from .pot file and store in numbered tempfiles)

sed -i.bak2 's/{0}/51a2/g;s/{1}/51a3/g;s/{2}/51a4/g' "$ausgabeverzeichnis/$prefix.$pot_datei"

anzahl_zeilen="$(sed -n "$=" "$ausgabeverzeichnis/$prefix.$pot_datei")"
echo $"Lese .pot-Datei: $anzahl_zeilen Zeilen."
ist_msgid=false
ist_plural=false
n=1
eintrag_nummer=1
while [ $n -le $anzahl_zeilen ]; do
    zeile="$(sed -n ${n}p "$ausgabeverzeichnis/$prefix.$pot_datei")"
    if [ "$( echo "$zeile" | cut -c-6 )" ==  "msgid " ]; then
        ist_msgid=true
        ist_plural=false
        while $ist_msgid; do
            echo "$zeile" >> "$temporaerdatei$eintrag_nummer.tmp"   #schreibe Zeile in temporärdatei_$eintrag_nummer (write line to temp file $temporärdatei_$eintrag_nummer)
            let $((n++))
            zeile="$(sed -n ${n}p "$ausgabeverzeichnis/$prefix.$pot_datei")"
            if [ "$(sed -n ${n}p "$ausgabeverzeichnis/$prefix.$pot_datei" | cut -c1 )" != '"' ] && [ "$(sed -n "${n}p" "$ausgabeverzeichnis/$prefix.$pot_datei" | cut -c-2 )" != 'x"' ]; then ist_msgid=false; fi
            if [ "$( echo "$zeile" | cut -c-13 )" ==  "msgid_plural " ]; then
                ist_plural=true
            fi
        done
        while $ist_plural; do
            echo "$zeile" >> "$temporaerdatei$eintrag_nummer.tmp"   #schreibe Zeile in temporärdatei_$eintrag_nummer (write line to temp file $temporärdatei_$eintrag_nummer)
            let $((n++))
            zeile="$(sed -n ${n}p "$ausgabeverzeichnis/$prefix.$pot_datei")"
            if [ "$(sed -n ${n}p "$ausgabeverzeichnis/$prefix.$pot_datei" | cut -c1 )" != '"' ] && [ "$(sed -n "${n}p" "$ausgabeverzeichnis/$prefix.$pot_datei" | cut -c-2 )" != 'x"' ]; then ist_plural=false; fi
        done
        let $((eintrag_nummer++))
    else
        let $((n++))
    fi
done

while read -r zielsprache <&3; do
    if ! echo "$zielsprache" | grep -E '^#' > /dev/null && [ "$zielsprache" != "$ausgangssprache" ]; then
        echo -e "\033[1;34m--------------------\n*  "$"Übersetze zu""" """$zielsprache""\n--------------------\033[0;37m"
        mkdir -p "$ausgabeverzeichnis/$zielsprache"
        ausgabedatei="$ausgabeverzeichnis/$zielsprache/$(echo "$pot_datei" | rev | cut -d. -f2- | rev).po"
#       textdatei="$ausgabeverzeichnis/$zielsprache/$(echo "$pot_datei" | rev | cut -d. -f2- | rev).txt"
        po_datei="$ausgabeverzeichnis/$zielsprache/$(echo "$pot_datei" | rev | cut -d. -f2- | rev).po.bak"
>"$ausgabedatei"
#>"$textdatei"

        #.po-Datei vom transifex Server holen (fetch .po file from transifex), save it to .po.bak
echo $"Hole Übersetzung vom transifex-Server"
        tx pull -f --minimum-perc 100 -l "$zielsprache" -r "$projektkennung.$ressourcenkennung" # --no-interactive
        if [ -s "$ausgabedatei" ]; then
            # skip this language, it is ready. Mark as processed in language list
            echo $"Sprache ist bereits zu 100% übersetzt. Wird übergangen."
            #sed -i "s/^"$zielsprache"$/#"$zielsprache"/" "$sprachenliste" # no longer needed since script skips 100% translated languages now.
            populategit
            continue
        else
            sleep 3  # workaround needed for new tx client
        fi
        tx pull -f -l "$zielsprache" -r "$projektkennung.$ressourcenkennung" # --no-interactive
        mv "$ausgabedatei" "$po_datei"
        # Fehlerhafte Zeilenendzeichen korrigieren: Zu finden sind gemischte Windows, Unix und MAC Zeilenenden in den .po Dateien von Transifex. Alle vor der Verarbeitung zu Unix konvertieren. (Change line endings from clasic apple and windows defaults to unix standard as needed)
        sed 's/\r$//' "$po_datei" | tr '\r' '\n' > "$po_datei-$$"
        mv "$po_datei-$$" "$po_datei"
        anzahl_zeilen="$(sed -n "$=" "$po_datei")"
echo $"Verarbeite po-Datei, $anzahl_zeilen Zeilen"
        ist_msgid=false
        ist_plural=false
        ist_msgstr=false
        ist_uebersetzbar=true
        n=1
        eintrag_nummer=1
        while [ $n -le $anzahl_zeilen ]; do
            zeile="$(sed -n ${n}p "$po_datei")"
            if [ "$( echo "$zeile" | cut -c1 )" ==  '#' ]; then
                echo "$zeile" >> "$ausgabedatei"                                        # schreibe Kopfeintrag, Developer notes, Kommentare etc. (Write comments)
                let $((n++))
                continue
            elif [ "$( echo -n "$zeile" | wc -c )" -eq  0 ]; then
                echo "$zeile" >> "$ausgabedatei"                                        # schreibe Leerzeilen (Write empty lines)
                let $((n++))
                continue
            elif [ "$( echo "$zeile" | cut -c-8 )" ==  "msgctxt " ]; then
                echo "$zeile" >> "$ausgabedatei"                                        # schreibe msgctxt-Zeilen (Write msgctxt lines)
                let $((n++))
                continue
            elif [ "$( echo "$zeile" | cut -c-6 )" ==  "msgid " ]; then
                ist_msgid=true                                                          # erkenne msgid-Einträge (recognise msgid entries)
                r=$n
                xzeile="$(sed -n ${r}p "$po_datei")"
                while [ "$( echo "$xzeile" | cut -c-6 )" !=  "msgstr" ]; do
                    if [ "$( echo "$xzeile" | cut -c-13 )" ==  "msgid_plural " ]; then ist_plural=true; fi    # erkenne zusätzliche Plural-Einträge (recognise msgid_plural entries)
                    let $((r++))
                    xzeile="$(sed -n ${r}p "$po_datei")"
                done
                ist_unuebersetzt=true
                if [ "$(sed -n "${r}p" "$po_datei" | sed 's/^msgstr\[*[[:digit:]]*\]* "//; s/"$//')" == "" ]; then    # erkenne schon übersetzte msgid-Einträge (recognise whether an entry is translated already)
                    let $((r++))
                    if [ "$(sed -n ${r}p "$po_datei" | cut -c1 )" != "" ]; then
                        if [ "$(sed -n "${r}p" "$po_datei" | sed 's/^msgstr\[*[[:digit:]]*\]* "//; s/"$//')" == "" ]; then
                            let $((r++))
                            if [ "$(sed -n ${r}p "$po_datei" | cut -c1 )" != "" ]; then
                                ist_unuebersetzt=false
                            else
                                n=$r
                            fi
                        else
                            ist_unuebersetzt=false
                        fi
                    else
                        n=$r
                    fi
                else
                        ist_unuebersetzt=false
                fi
            elif [ "$( echo "$zeile" | cut -c-6 )" ==  "msgstr" ]; then
                ist_msgstr=true                                                            # erkenne msgstr-Zeilen (recognise msgstr entries)
            fi
            if $ist_unuebersetzt; then
                anzahl_msgzeilen="$(sed -n "$=" "$temporaerdatei$eintrag_nummer.tmp")"
                for (( p=1; p<=$anzahl_msgzeilen; p++ )); do
                    q=$((p+1))
                    zeile="$(sed -n ${p}p "$temporaerdatei$eintrag_nummer.tmp")"
                    if [ "$( echo "$zeile" | cut -c-7 )" == 'msgid "' ]; then
                        if [ "$( echo "$zeile" | cut -c-8 )" != 'msgid ""' ]; then
                            uebersetzungszeile="$(echo "$zeile" | sed 's/^msgid "//; s/"$//')"              # Übersetzungszeile beginnen (start translation line)
                        fi
                        if ! $ist_plural; then
                            ausgabezeile='msgstr "'                                                         # Ausgabezeile beginnen (start output line)
                        else
                            ausgabezeile='msgstr[0] "'                                                      # Ausgabezeile beginnen (start output line)
                        fi
                        echo "$zeile" >> "$ausgabedatei"                                                    # schreibe msgid-Einträge" (write msgid entries to output file)
                    elif [ "$( echo "$zeile" | cut -c-14 )" == 'msgid_plural "' ]; then
                        uebersetzungszeile="$(echo "$zeile" | sed 's/^msgid_plural "//; s/"$//')"           # Übersetzungszeile beginnen (start translation line)
                        ausgabezeile=$ausgabezeile$(printf "\r")'msgstr[1] "'                               # Ausgabezeile beginnen (start output line)
                        echo "$zeile" >> "$ausgabedatei"                                                    # schreibe msgid-Einträge" (write msgid entries to output file)
                        ist_plural=false
                    elif [ "$( echo "$zeile" | cut -c1 )" == '"' ]; then
                        if [ -n "$uebersetzungszeile" ]; then
                            uebersetzungszeile="$uebersetzungszeile $(echo "$zeile" | sed 's/^"//; s/"$//')"    # Übersetzungszeile fortsetzen (continue translation line)
                        else
                            uebersetzungszeile="$(echo "$zeile" | sed 's/^"//; s/"$//')"                    # Übersetzungszeile fortsetzen (continue translation line)
                        fi
                        echo "$zeile" >> "$ausgabedatei"                                                    # schreibe übersetzbare msgid-Folgeeinträge" (write translatable msgid followup lines)
                    elif [ "$(echo "$zeile" | cut -c-2 )" == 'x"' ]; then
                        if [ "$ausgabezeile" == 'msgstr "' ] || [ "$ausgabezeile" == 'msgstr[0] "' ] || [ "${ausgabezeile:$((${#ausgabezeile}-11))}" == 'msgstr[1] "' ]; then ausgabezeile="$ausgabezeile$(printf "\x22")"; fi                # erste Ausgabezeile vor Anhängen schließen (close first line of output before append next part)
                        if [ -n "$uebersetzungszeile" ]; then
echo -e "\n\033[1;37m"
echo "$uebersetzungszeile"
echo -en "\033[0;37m"
                            uebersetzen                                                                     # Bisher vorhandene Übersetzungszeile übersetzen lassen (translate what we have by now in translation line)
echo "$uebersetzung"
                            ausgabezeile="$ausgabezeile$(printf "\r\x22")$uebersetzung$(printf "\x22")"     # Übersetzung an Ausgabezeile anhängen (append translation to output line)
                            uebersetzungszeile=""                                                           # Übersetzungszeile leeren (clear translation line)
                        fi
                        ausgabezeile="$ausgabezeile$(printf "\r")$(echo -n "$zeile" | sed 's/^.//' )"       # Zeileninhalt an Ausgabezeile anhängen (append recent line to output line)
                        echo "$zeile" | cut -c2- >> "$ausgabedatei"                                         # schreibe nicht übersetzbare msgid-Folgeeinträge" (write non-translatable followup msgid entries to output file)
                    fi

                    if [ "$(sed -n ${q}p "$temporaerdatei$eintrag_nummer.tmp" | cut -c1 )" != '"' ] && [ "$(sed -n "${q}p" "$temporaerdatei$eintrag_nummer.tmp" | cut -c-2 )" != 'x"' ]; then
                        if [ "$ausgabezeile" == 'msgstr "' ] || [ "$ausgabezeile" == 'msgstr[0] "' ] || [ "${ausgabezeile:$((${#ausgabezeile}-11))}" == 'msgstr[1] "' ]; then ausgabezeile="$ausgabezeile$(printf "\x22")"; fi                # erste Ausgabezeile vor Anhängen schließen
                        if [ -n "$uebersetzungszeile" ]; then
echo -e "\n\033[1;37m"
echo "$uebersetzungszeile"
echo -en "\033[0;37m"
                            uebersetzen                                                                     # Übersetzungszeile übersetzen lassen, (translate rest of translation line)
echo "$uebersetzung"
                            ausgabezeile="$ausgabezeile$(printf "\r\x22")$uebersetzung$(printf "\x22")"     # Übersetzung an Ausgabezeile anhängen (append translation to output line)
                            uebersetzungszeile=""                                                           # Übersetzungszeile leeren (clear translation line)
                        fi
                        if ! $ist_plural; then
                            echo "$ausgabezeile" >> "$ausgabedatei"                                         # schreibe Ausgabezeile in Datei (write output line to file)
                            ausgabezeile=""                                                                 # Ausgabezeile leeren (clear output line)
                            ist_msgid=false
                            ist_unuebersetzt=false
                        fi
                    fi
                done
                ist_plural=false
                ist_unuebersetzt=false
                let $((eintrag_nummer++))
            else                                                                            # treat entries alredy translated
                if [ "$( echo "$zeile" | cut -c-5 )" ==  "msgid" ]; then
                    while [ "$( echo "$zeile" | cut -c-6 )" !=  "msgstr" ]; do
                        zeile="$(sed -n ${n}p "$po_datei")"
                        echo "$zeile" >> "$ausgabedatei"                                    # schreibe kompletten msgid-Eintrag" (write complete msgid entry to output file)
                        let $((n++))
                    done
                fi
                if [ "$( echo "$zeile" | cut -c-6 )" ==  "msgstr" ]; then
                    while [ -n "$( echo "$zeile" | cut -c-6 )" ]; do
                        zeile="$(sed -n ${n}p "$po_datei")"
                        echo "$zeile" >> "$ausgabedatei"                                    # schreibe kompletten msgstr-Eintrag" (write complete msgstr entry to output file)
                        let $((n++))
                    done
                fi
                ist_plural=false
                ist_unuebersetzt=false
                let $((eintrag_nummer++))
            fi
        done

        nachbearbeiten

        #todo: check result (formal) before uploading

        # markiere Sprache als "Erledigt". (mark language as processed in language file)
        #sed -i "s/^"$zielsprache"$/#"$zielsprache"/" "$sprachenliste"        # no longer needed since script skips 100% translated languages// was needed to resume with next language not already done after any interuption of processing.
        
        if ! $dry_run; then
            hochladen
        fi

        populategit

        #if ! $dry_run; then
            # .mo-Datei zum gitlab server hochladen (transfer .mo and .po file to gitlab)
            # gitupload
            # (not implemented yet)
        #fi

        # gebe dem Übersetzungsdienst eine Verschnaufpause, damit wir nicht blockiert werden (give translation service some additional spare time in order not to block us soon)
        zeitkonstante=3
        #zeitkonstante=5
#       zeitkonstante=$(($(echo ${RANDOM:0-1} | sed 's/^ /0/')*10+31))
echo -e "\033[1;32m*  "$"Übersetzung zu ""$zielsprache "$"abgeschlossen.""\033[0;32m "$"Fortsetzung in ""$zeitkonstante "$"Sekunden. Taste »b« zum Beenden.""\033[0;37m"
        while :; do
            read -rsn1 -t $zeitkonstante entscheidung   # wait some additional seconds befor connecting online translation service again while asking
                                                        # user to stop processing.
            if [ "$entscheidung" = "b" ] || [ "$entscheidung" = "B" ]; then
                echo -e "*  "$"Die Übersetzung wurde unterbrochen.""\n*  \033[1;37m"$"Zum Fortsetzen mit der nächsten Sprache Skript erneut starten.""\033[0;37m"
                exit 0
            elif [ "$entscheidung" = "" ]; then
                break
            fi
        done
    fi
done 3< "$arbeitsverzeichnis/$sprachenliste"


exit 0
